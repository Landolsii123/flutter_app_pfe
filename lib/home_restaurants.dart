import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'styles.dart';
import 'signup.dart';
import 'restaurant.dart';
import 'menu.dart';
import 'all_restaurants.dart';
import 'dart:async';
import 'home.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:geolocator/geolocator.dart';
import 'http_service.dart';
import 'dart:math' show cos, sqrt, asin;
import 'package:flutter_mapbox_navigation/flutter_mapbox_navigation.dart';
import 'package:healthy_food_locator/menu_details.dart';


class HomeRestaurant extends StatefulWidget {
  @override
  _HomeRestaurant createState() => _HomeRestaurant();
}


class _HomeRestaurant extends State<HomeRestaurant> { 

  Position currentPosition;

  var menus={};
        

  @override
  void initState() {
    super.initState();
    getMenuByRestaurant().then((data) {
      print(data);
      setState(() {
        for(int i=0; i<data.length ;i++)
        menus[i] = data[i];
      });
    });
   
    getCurrentLocation().then((position) {
      setState(() {
        print (position);
        currentPosition = position;
      });
    });
  }

  calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((lon2 - lon1) * p))/2;
    return (12742 * asin(sqrt(a))).toStringAsFixed(1);
  }

 
  @override 
  Widget build (BuildContext context) { 
  MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold (
      body:SingleChildScrollView(
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 80,
            ),
            //Menus by calories of the user
            Container(
              padding: EdgeInsets.only(left:5,bottom: 10, top:20),
              child: Text(
                'Best menus for you ',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: colorCustom
                ),
              ),
            ),
            Container(
              height:200,
              child:((menus!=null)&&(currentPosition!=null))
              ? ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: menus.length,
                itemBuilder: (BuildContext context, int index) {
                  return  
                  Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        print('Card tapped.');
                      },
                      child: Container(
                        
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        width: MediaQuery.of(context).size.width*0.6,
                        child:Stack(
                          children:<Widget>[
                            Container(
                              height: 200,
                              decoration: BoxDecoration(
                                image:DecorationImage(image: NetworkImage(url+'5012/images/'+menus[index]['image']),
                                  fit: BoxFit.fill,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                              ),
                            ),
                            Align(
                              alignment: Alignment(0,0.85),
                              child: Container(
                                padding: EdgeInsets.only(left:20,top:12,right:12,bottom: 12),
                                width: MediaQuery.of(context).size.width*0.55,
                                height: 78,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                  color:Colors.white.withOpacity(0.9),
                                ),
                                child:Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 8,
                                          child: Text(
                                            '${menus[index]['name']}',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: Text(
                                            '${menus[index]['price']} TND',
                                            //'${calculateDistance(currentPosition.latitude, currentPosition.longitude, double.parse(menus[index]['location']['latitude']),double.parse(menus[index]['location']['longitude']))}',
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    
                                  // Padding(padding: EdgeInsets.only(bottom: 5)),
                                    Divider(
                                      color: Colors.black,
                                      endIndent: 10,
                                      thickness: 0,
                                    ),
                                    Row(
                                        children: <Widget>[
                                          Expanded(
                                          flex: 5,
                                          child: Text(
                                            '${menus[index]['calories']} Cal',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                            '${menus[index]['preparation_time']} Min',
                                            //'${calculateDistance(currentPosition.latitude, currentPosition.longitude, double.parse(menus[index]['location']['latitude']),double.parse(menus[index]['location']['longitude']))}',
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                        ],
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      ),
                    
                    ),
                  );
                })
              : Center(
                child: Text('nothing'),
              ), 
            ),  
            //last added menus
            Container(
              padding: EdgeInsets.only(left:5,bottom: 10,top:10),
              child: Text(
                'New menus',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: colorCustom
                ),
              ),
            ),
            Container(
              height:200,
              child:((menus!=null)&&(currentPosition!=null))
              ? ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: menus.length,
                itemBuilder: (BuildContext context, int index) {
                  return  
                  Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        print('Card tapped.');
                      },
                      child: Container(
                        
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        width: MediaQuery.of(context).size.width*0.6,
                        child:Stack(
                          children:<Widget>[
                            Container(
                              height: 200,
                              decoration: BoxDecoration(
                                image:DecorationImage(image: NetworkImage(url+'5012/images/'+menus[index]['image']),
                                  fit: BoxFit.fill,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                              ),
                            ),
                            Align(
                              alignment: Alignment(0,0.85),
                              child: Container(
                                padding: EdgeInsets.only(left:20,top:12,right:12,bottom: 12),
                                width: MediaQuery.of(context).size.width*0.55,
                                height: 78,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                  color:Colors.white.withOpacity(0.9),
                                ),
                                child:Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 8,
                                          child: Text(
                                            '${menus[index]['name']}',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: Text(
                                            '${menus[index]['price']} TND',
                                            //'${calculateDistance(currentPosition.latitude, currentPosition.longitude, double.parse(menus[index]['location']['latitude']),double.parse(menus[index]['location']['longitude']))}',
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    
                                  // Padding(padding: EdgeInsets.only(bottom: 5)),
                                    Divider(
                                      color: Colors.black,
                                      endIndent: 10,
                                      thickness: 0,
                                    ),
                                    Row(
                                        children: <Widget>[
                                          Expanded(
                                          flex: 5,
                                          child: Text(
                                            '${menus[index]['calories']} Cal',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                            '${menus[index]['preparation_time']} Min',
                                            //'${calculateDistance(currentPosition.latitude, currentPosition.longitude, double.parse(menus[index]['location']['latitude']),double.parse(menus[index]['location']['longitude']))}',
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                        ],
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      ),
                    
                    ),
                  );
                })
              : Center(
                child: Text('nothing'),
              ), 
            ),  
            //most sold menu per review
            Container(
              padding: EdgeInsets.only(left:5,bottom: 10, top:20),
              child: Text(
                'Best seller menus',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: colorCustom
                ),
              ),
            ),
            Container(
              height:200,
              child:((menus!=null)&&(currentPosition!=null))
              ? ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: menus.length,
                itemBuilder: (BuildContext context, int index) {
                  return  
                  Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        print('Card tapped.');
                      },
                      child: Container(
                        
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        width: MediaQuery.of(context).size.width*0.6,
                        child:Stack(
                          children:<Widget>[
                            Container(
                              height: 200,
                              decoration: BoxDecoration(
                                image:DecorationImage(image: NetworkImage(url+'5012/images/'+menus[index]['image']),
                                  fit: BoxFit.fill,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                              ),
                            ),
                            Align(
                              alignment: Alignment(0,0.85),
                              child: Container(
                                padding: EdgeInsets.only(left:20,top:12,right:12,bottom: 12),
                                width: MediaQuery.of(context).size.width*0.55,
                                height: 78,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                  color:Colors.white.withOpacity(0.9),
                                ),
                                child:Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 8,
                                          child: Text(
                                            '${menus[index]['name']}',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: Text(
                                            '${menus[index]['price']} TND',
                                            //'${calculateDistance(currentPosition.latitude, currentPosition.longitude, double.parse(menus[index]['location']['latitude']),double.parse(menus[index]['location']['longitude']))}',
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    
                                  // Padding(padding: EdgeInsets.only(bottom: 5)),
                                    Divider(
                                      color: Colors.black,
                                      endIndent: 10,
                                      thickness: 0,
                                    ),
                                    Row(
                                        children: <Widget>[
                                          Expanded(
                                          flex: 5,
                                          child: Text(
                                            '${menus[index]['calories']} Cal',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                            '${menus[index]['preparation_time']} Min',
                                            //'${calculateDistance(currentPosition.latitude, currentPosition.longitude, double.parse(menus[index]['location']['latitude']),double.parse(menus[index]['location']['longitude']))}',
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                        ],
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      ),
                    
                    ),
                  );
                })
              : Center(
                child: Text('nothing'),
              ), 
            ),  
          
          ],
        ),
      ),
    );
  } 
}


