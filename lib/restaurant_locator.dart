import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'styles.dart';
import 'restaurant.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:geolocator/geolocator.dart';
import 'http_service.dart';
import 'dart:math' show cos, sqrt, asin;
import 'package:flutter_mapbox_navigation/flutter_mapbox_navigation.dart';


class RestaurantLocator extends StatefulWidget {
  @override
  _RestaurantLocator createState() => _RestaurantLocator();
}


class _RestaurantLocator extends State<RestaurantLocator> { 

  Position currentPosition;

  var restaurants={};

  List<Marker> markerss;


  MapboxNavigation _directions;
    final _origin = Location(name: "Sousse", latitude: 35.22, longitude: -101.83);
  //final _origin = Location(name: "City Hall", latitude: 42.886448, longitude: -78.878372);
    final _destination = Location(
      name: "Monastir", latitude: 35.21, longitude: -101.83);
        

  @override
  void initState() {
    super.initState();

    markerss=List<Marker>();
    
    getAllRestaurants().then((data) {
      setState(() {
        _directions = MapboxNavigation(onRouteProgress: (arrived) async {});
        for(int i=0;i<data.length;i++){
          restaurants[i]=data[i];
          markerss.add(
            Marker(
            width: 40,
            height: 40,
            point: LatLng(double.parse(restaurants[i]['location']['latitude']), double.parse(restaurants[i]['location']['longitude'])),
            builder: (context) => new Container(
                  child: IconButton(
                    icon: Icon(Icons.location_on),
                    color: Colors.blue,
                    iconSize: 45.0,
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) => CupertinoAlertDialog(
                          title: new Text("Wanna Get there ?"),
                          actions: <Widget>[
                            CupertinoDialogAction(
                              isDefaultAction: true,
                              child: Text("Let's go"),
                              onPressed: ()  async{
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) => CupertinoAlertDialog(
                                        title: new Text("Wanna Get there ?"),
                                        actions: <Widget>[
                                          CupertinoDialogAction(
                                            isDefaultAction: true,
                                            child: Text("Let's go"),
                                            onPressed: ()  async{
                                              await _directions.startNavigation(
                                                origin:_origin,// Location(name: restaurants[i]['name'], latitude: currentPosition.latitude, longitude: currentPosition.longitude)
                                                destination: _destination,// Location(name: restaurants[i]['name'], latitude: double.parse(restaurants[0]['location']['latitude']), longitude: double.parse(restaurants[0]['location']['longitude']))
                                                mode: NavigationMode.drivingWithTraffic,
                                                simulateRoute: true, language: "German", units: VoiceUnits.metric
                                              );
                                            },
                                          ),
                                          CupertinoDialogAction(
                                            child: Text("no, It's okay",style: TextStyle(color:Colors.red),),
                                            onPressed: (){
                                              Navigator.of(context, rootNavigator: true).pop();
                                            },
                                          )
                                        ],
                                      )
                                    );
                              },
                            ),
                            CupertinoDialogAction(
                              child: Text("no, It's okay",style: TextStyle(color:Colors.red),),
                              onPressed: (){
                                Navigator.of(context, rootNavigator: true).pop();
                              },
                            )
                          ],
                        )
                      );
                    },
                                
                  )
                ),
          ),
        
          );
        }

        });
    });

    getCurrentLocation().then((position) {
      setState(() {
        print (position);
        currentPosition = position;
        markerss.add(
          Marker(
            width: 40,
            height: 40,
            point: LatLng(currentPosition.latitude, currentPosition.longitude),
            builder: (context) => new Container(
                  child: IconButton(
                    icon: Icon(Icons.my_location),
                    color: Colors.blue,
                    iconSize: 25.0,
                    onPressed: () {
                    },         
                  )
                ),
          ),
        
        );
      });
    });
  }

  calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((lon2 - lon1) * p))/2;
    return (12742 * asin(sqrt(a))).toStringAsFixed(1);
  }

  var points = <LatLng>[
        new LatLng(35.22, -101.83),
        new LatLng(32.77, -96.79),
        new LatLng(29.76, -95.36),
        new LatLng(29.42, -98.49),
        new LatLng(35.22, -101.83),
  ];
 
  @override 
  Widget build (BuildContext context) { 
  MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold (
      body:currentPosition != null ? SafeArea(
        child:Container(
          child: Stack(
            children: <Widget>[
              FlutterMap(
                    options: MapOptions(
                        center: LatLng(currentPosition.latitude, currentPosition.longitude), minZoom: 5, zoom: 15),
                    layers: [
                      TileLayerOptions(
                        urlTemplate:
                            "https://api.mapbox.com/styles/v1/landolsii123/ck882htit03pn1iphovobfy6l/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibGFuZG9sc2lpMTIzIiwiYSI6ImNrN3p6NmQzcjAwaHozZW9kdXlqbjdyMXkifQ.Qn-dRKqwv7QEO-Mt8HtuRw",
                        additionalOptions: {
                            'accessToken':'pk.eyJ1IjoibGFuZG9sc2lpMTIzIiwiYSI6ImNrN3p6NmQzcjAwaHozZW9kdXlqbjdyMXkifQ.Qn-dRKqwv7QEO-Mt8HtuRw',
                            'id': 'mapbox.mapbox-streets-v7'
                        },
                      ),
                      MarkerLayerOptions(
                        markers: markerss,
                      ),
                      
                    ],
                  ),
              Align(
                alignment: Alignment(0,0.9),
                child: Container(
                  height:200,
                  
                  child:((restaurants!=null)&&(currentPosition!=null))
                    ? ListView.builder(
                    scrollDirection: Axis.horizontal,
                      itemCount: restaurants.length,
                      itemBuilder: (BuildContext context, int index) {
                        return  
                        Card(
                          child: InkWell(
                            splashColor: Colors.blue.withAlpha(30),
                            onTap: () {
                              print(restaurants[index]['_id']);
                              Navigator.push(context, MaterialPageRoute(
                                builder: (context) => Restaurant(),
                                settings: RouteSettings(arguments: restaurants[index]['_id']),
                                )
                              );
                            },
                            child: Container(
                              
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                              ),
                              width: MediaQuery.of(context).size.width*0.6,
                              child:Stack(
                                children:<Widget>[
                                  Container(
                                    height: 200,
                                    decoration: BoxDecoration(
                                      image:DecorationImage(image: NetworkImage(url+'5011/images/'+restaurants[index]['restaurant_image']),
                                        fit: BoxFit.fill,
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(5)),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment(0,0.85),
                                    child: Container(
                                      padding: EdgeInsets.only(left:20,top:12,right:12,bottom: 12),
                                      width: MediaQuery.of(context).size.width*0.8,
                                      height: 90,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        color:Colors.white,
                                      ),
                                      child:Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 8,
                                                child: Text(
                                                  '${restaurants[index]['name']}',
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily: 'Montserrat',
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 3,
                                                child: Text(
                                                  '${calculateDistance(currentPosition.latitude, currentPosition.longitude, double.parse(restaurants[index]['location']['latitude']),double.parse(restaurants[index]['location']['longitude']))}Km',
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily: 'Montserrat',
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          
                                        // Padding(padding: EdgeInsets.only(bottom: 5)),
                                          Divider(
                                            color: Colors.black,
                                            endIndent: 10,
                                            thickness: 0,
                                            
                                          ),
                                          Row(
                                              children: <Widget>[
                                                Container(
                                                  width: MediaQuery.of(context).size.width*0.08,
                                                  child: Icon(Icons.star,color:Colors.yellow),
                                                ),
                                                Container(
                                                  width: MediaQuery.of(context).size.width*0.15,
                                                  child: Text(
                                                    '${restaurants[index]['reviews']}',
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                    ),
                                                  ),
                                                ),
                                                VerticalDivider(),
                                                Container(
                                                  width: MediaQuery.of(context).size.width*0.08,
                                                  child: Icon(Icons.timer),
                                                ),
                                                Container(
                                                  width: MediaQuery.of(context).size.width*0.15,
                                                  child: Text(
                                                    'Opened',
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      color: CupertinoColors.activeGreen,
                                                      fontWeight: FontWeight.bold
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ),
                          
                          ),
                        );
                      })
                    : Center(
                      child: Text('nothing'),
                    ), 
                  ),
              ),
              Align(
                alignment: Alignment.topCenter,
                
                child:Container(
                  margin: EdgeInsets.only(left:25,top:20, right: 25),
                  child: CupertinoTextField(
                    placeholder: "Search for restaurant",
                    style: TextStyle(
                      fontFamily: 'Montserrat'
                    ),
                    
                  ),
                ),
              ),
            ],
          ),
        ),
      ):
      Center(
        child: CircularProgressIndicator(),
      ),
    );
  } 
}


class NormalView extends State<RestaurantLocator> { 

 
  @override
  void initState() {
    super.initState();

  }
 
  @override 
  Widget build (BuildContext context) { 
  MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold (
      body:Text('Nooooooormal')
    );
  } 
}

class MappView extends State<RestaurantLocator> { 

 
  @override
  void initState() {
    super.initState();

  }
 
  @override 
  Widget build (BuildContext context) { 
  MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold (
      body:Text('maaaaaap')
    );
  } 
}

