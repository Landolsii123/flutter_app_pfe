import 'package:flutter/cupertino.dart' ;
import 'package:flutter/material.dart';
import 'styles.dart';
import 'http_service.dart';
import 'restaurant.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:math' show cos, sqrt, asin;




class AllRestaurants extends StatefulWidget {
  @override
  _AllRestaurants createState() => _AllRestaurants();
}

class _AllRestaurants extends State<AllRestaurants>{
  var restaurants={};
  
  
  
  Position currentPosition;
  //double distaanes;

  calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((lon2 - lon1) * p))/2;
    return (12742 * asin(sqrt(a))).toStringAsFixed(1);
  }

  void initState() {
    Future.delayed(const Duration(seconds: 0),(){
      
      getAllRestaurants().then((data) {
        setState(() {
          for(int i=0;i<data.length;i++){
            restaurants[i]=data[i];
          }
        });
      });
    
      getCurrentLocation().then((position) {
        setState(() {
          currentPosition = position;
        });
      });
      
    });
  super.initState();
  }

  
  @override 
  Widget build (BuildContext context){
    MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold(
      //backgroundColor: CupertinoColors.systemGrey6,
        body:SingleChildScrollView(
          child:Column(
             crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[ 
              SizedBox(
                height: 40,
              ),
              //search bar  
              Container(
                height: 35,
                margin: EdgeInsets.only(left:20,top:10,bottom: 5),
                child: Text(
                  'Restaurants',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    fontFamily: 'Montserrat'
                  ),
                ),
              ),
             
              Container(
                margin: EdgeInsets.fromLTRB(5, 15, 5,0),
                child: CupertinoTextField(
                  placeholder: 'Search ..',
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height*0.75,
                padding: EdgeInsets.only(left:10,right: 10),
                  child: ((restaurants!=null)&&(currentPosition!=null))
                      ? ListView.builder(
                          itemCount: restaurants.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                            child: InkWell(
                            splashColor: Colors.blue.withAlpha(30),
                            onTap: () {
                              print(restaurants[index]['_id']);
                              Navigator.push(context, MaterialPageRoute(
                                builder: (context) => Restaurant(),
                                settings: RouteSettings(arguments: restaurants[index]['_id']),
                                )
                              );
                            },
                            child: Container(
                              height: 150,
                              child:Row(
                                //crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1, 
                                    child: Column(
                                      children: <Widget>[
                                        
                                        Image(
                                          image: NetworkImage(url+'5011/images/'+restaurants[index]['restaurant_image']),
                                          fit: BoxFit.fitHeight,
                                          width: 100,
                                          height: 100,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(5, 15, 5, 0)
                                        ),
                                        Text(
                                          '${calculateDistance(currentPosition.latitude, currentPosition.longitude, double.parse(restaurants[index]['location']['latitude']),double.parse(restaurants[index]['location']['longitude']))} km',
                                          style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Montserrat'
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex:3, 
                                    child: Column(
                                      children: <Widget>[
                                        Padding(padding: EdgeInsets.only(top:10)),
                                        Expanded(
                                          child:Row(
                                            crossAxisAlignment :CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                              Expanded(
                                                flex:8,
                                                child: Text(
                                                  '${restaurants[index]['name']}',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize:18,
                                                    color:colorCustom,
                                                    fontFamily: 'Montserrat'
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex:2,
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  margin: EdgeInsets.only(right:10),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(color: Colors.green),
                                                    borderRadius: BorderRadius.circular(5),
                                                  ),
                                                  padding: EdgeInsets.all(10),
                                                  width: 50,
                                                  height: 40,
                                                  child:Text(
                                                    '${restaurants[index]['reviews']}',
                                                    style: TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize:10,
                                                      color:Colors.black45,
                                                      fontFamily: 'Montserrat'
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 2,
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Expanded(
                                          child:Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                              Expanded(child: 
                                                Text(
                                                    '${restaurants[index]['description']}', 
                                                    style:TextStyle(
                                                      fontSize:14,
                                                      fontFamily: 'Montserrat'
                                                    )
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height:5
                                        ),
                                        Expanded(
                                          child:Row(
                                            children: <Widget>[
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text(
                                                      'Min order',
                                                      style: TextStyle(
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),
                                                    Text(
                                                      '4 TND',
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize:16,
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),

                                                  ],
                                                ),
                                              ),
                                              VerticalDivider(
                                                endIndent: 10,
                                                color: CupertinoColors.systemGrey4,
                                                thickness: 1,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text(
                                                      'Delivery time',
                                                      style:TextStyle(
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),
                                                    Text(
                                                      '30-45min',
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize:16,
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  
                                ],
                              )
                            )
                          ),
                        );
                      })
                  : Center(
                  child: CircularProgressIndicator(),
                ),
              ),
           ],
          ),
        ),
    );
  }
}

/* second card

Card(
                            child: InkWell(
                            splashColor: Colors.blue.withAlpha(30),
                            onTap: () {
                              print(restaurants[index]['_id']);
                              Navigator.push(context, MaterialPageRoute(
                                builder: (context) =>Restaurant(),
                                settings: RouteSettings(arguments: restaurants[index]['_id']),
                                )
                              );
                            },
                            child: Container(
                              height: 150,
                              child:Row(
                                //crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1, 
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(5, 10, 5, 10)
                                        ),
                                        Image(
                                          image: NetworkImage('http://10.0.2.2:5011/images/'+restaurants[index]['restaurant_image']),
                                          fit: BoxFit.fitHeight,
                                          width: 100,
                                          height: 60,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(5, 20, 5, 10)
                                        ),
                                        Text(
                                          '3,1 km',
                                          style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex:3, 
                                    child: Column(
                                      children: <Widget>[
                                        Padding(padding: EdgeInsets.only(top:10)),
                                        Expanded(
                                          child:Row(
                                            crossAxisAlignment :CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                              Expanded(
                                                flex:8,
                                                child: Text(
                                                  '${restaurants[index]['name']}',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize:18,
                                                    color:colorCustom
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex:2,
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  margin: EdgeInsets.only(right:10),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(color: Colors.green),
                                                    borderRadius: BorderRadius.circular(5),
                                                  ),
                                                  padding: EdgeInsets.all(10),
                                                  width: 50,
                                                  height: 40,
                                                  child:Text(
                                                    '4,5',
                                                    style: TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize:14,
                                                      color:Colors.black45,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 2,
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Expanded(
                                          
                                          child:Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                              Expanded(child: 
                                              Text(
                                                    'A lot of ingredients that can make you fucked up in the brain', 
                                                    style:TextStyle(
                                                      fontSize:14,
                                                      
                                                    )
                                                  ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height:5
                                        ),
                                        Expanded(
                                          child:Row(
                                            children: <Widget>[
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text('Min order'),
                                                    Text(
                                                      '30€',
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize:16,
                                                        color:CupertinoColors.systemGrey
                                                      ),
                                                    ),

                                                  ],
                                                ),
                                              ),
                                              VerticalDivider(
                                                endIndent: 10,
                                                color: CupertinoColors.systemGrey4,
                                                thickness: 1,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text('Delivery time'),
                                                    Text(
                                                    '30-45min',
                                                    style: TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize:16,
                                                      color:CupertinoColors.systemGrey
                                                    ),
                                                  ),
                                                
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  
                                ],
                              )
                            )
                          ),
                        );

*/

/*
Card(
                child: InkWell(
                  splashColor: Colors.blue.withAlpha(30),
                  onTap: () {
                    print('Card tapped.');
                  },
                  child: Container(
                    height: 150,
                    child:Row(
                      //crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 1, 
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(5, 10, 5, 10)
                              ),
                              Image(
                                image: AssetImage('assets/small-logo.png'),
                                fit: BoxFit.fitHeight,
                                width: 100,
                                height: 60,
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(5, 20, 5, 10)
                              ),
                              Text(
                                '3,1 km',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex:3, 
                          child: Column(
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(top:10)),
                              Expanded(
                                child:Row(
                                  crossAxisAlignment :CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                    Expanded(
                                      flex:8,
                                      child: Text(
                                        'Royal Plate',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize:18,
                                          color:colorCustom
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex:2,
                                      child: Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(right:10),
                                        decoration: BoxDecoration(
                                          border: Border.all(color: Colors.green),
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        padding: EdgeInsets.all(10),
                                        width: 50,
                                        height: 40,
                                        child:Text(
                                          '4,5',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:14,
                                            color:Colors.black45,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 2,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Expanded(
                                
                                child:Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                    Expanded(child: 
                                    Text(
                                          'A lot of ingredients that can make you fucked up in the brain', 
                                          style:TextStyle(
                                            fontSize:14,
                                            
                                          )
                                        ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height:5
                              ),
                              Expanded(
                                child:Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text('Min order'),
                                          Text(
                                            '30€',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize:16,
                                              color:CupertinoColors.systemGrey
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                    VerticalDivider(
                                      endIndent: 10,
                                      color: CupertinoColors.systemGrey4,
                                      thickness: 1,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text('Delivery time'),
                                          Text(
                                          '30-45min',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:16,
                                            color:CupertinoColors.systemGrey
                                          ),
                                        ),
                                      
                                        ],
                                      ),
                                    ),
                                   ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        
                      ],
                    )
                  )
                ),
              ), 
*/
