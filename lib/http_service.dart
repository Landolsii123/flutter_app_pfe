import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

var url="http://172.17.119.250:";
  loggedInUser()async {
    final prefs = await SharedPreferences.getInstance();
    var user = prefs.getString('user');
    if(user==null){
      return false;
    }
    else {
      return true;
    }
  }

  getCurrentLocation() async{
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);   print(position);
    return position;
  }
  
Future signIn(String email,String password) async{
  final prefs = await SharedPreferences.getInstance();
  //final http.Response response = await http.get('https://jsonplaceholder.typicode.com/todos');
  final http.Response response = await http.post(
    url+'5010/api/auth/login',
    body: {
      'email': email,
      'password':password
    },
  );
  print(json.decode(response.body));
  if (response.statusCode == 200) {
    prefs.setString('user', response.body);
    return true;
  }
  else{
    return false;
  }

}

getUser(id)async{
  var response = await http.get(url+'5010/api/auth/'+id);
  return json.decode(response.body);
}

getAllRestaurants() async{
  var response = await http.get(url+'5011/api/restaurant/all');
  return json.decode(response.body);
}

getOneRestaurant(id) async{
  var response = await http.get(url+'5011/api/restaurant/'+id);
  return json.decode(response.body);
}

getMenuByRestaurant() async{
  var response = await http.get(url+'5012/api/menu/all');
  return json.decode(response.body);
}

getMenuDetails(id) async {
  var response = await http.get(url+'5012/api/menu/'+id);
  return json.decode(response.body);
}

getAllIngredients() async{
  print('aaaaaaaaaaaaaa');
  var response = await http.get(url+'5013/api/ingredient/all');
  print(json.decode(response.body));
  return json.decode(response.body);
}

Future addMeasures(String userId,String height,String weight,String neck,String waist,String hip) async{
  
  //final http.Response response = await http.get('https://jsonplaceholder.typicode.com/todos');
  final http.Response response = await http.post(
    url+'5014/api/measures/add',
    body: {
      'userId':userId,
      'height':height,
      'weight':weight,
      'neck':neck,
      'waist':waist,
      'hip':hip,
    },
  );
  print(json.decode(response.body));
  if (response.statusCode == 200) {
    var reeees=json.decode(response.body);
    final http.Response responseMeasures = await http.post(
      url+'5010/api/auth/'+userId,
      body: {
        'measures':reeees['data']['_id'],
      },
    );
    return true;
  }
  else{
    return false;
  }

}


/*
Future <User> signIn(String email, String password)async {
  print('yyyyyyyyyyyyyyyyyy');
  final http.Response response = await http.post(
    'http://localhost:5010/api/auth/login',
    
    body: jsonEncode(<String, String>{
      'email': email,
      'password':password
    }),
  );
  if (response.statusCode == 201) {
    print(json.decode(response.body));
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return json.decode(response.body);
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

class User {
    final int id;
  final String email;
  final String password;

  User({this.id, this.email,this.password});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      email: json['email'],
      password: json['password']
    );
  }
}*/