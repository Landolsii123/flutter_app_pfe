import 'package:flutter/cupertino.dart' ;
import 'package:flutter/material.dart';
import 'package:healthy_food_locator/http_service.dart';
import 'package:healthy_food_locator/menu.dart';
import 'styles.dart';
import 'create_menu.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_mapbox_navigation/flutter_mapbox_navigation.dart';
import 'menu_details.dart';


class Restaurant extends StatefulWidget {
  @override
  _Restaurant createState() => _Restaurant();
}

class _Restaurant extends State<Restaurant>{

  var restaurant ;
  var id;
  var menus=[];
  Position currentPosition;
  MapboxNavigation _directions;
    final _origin = Location(name: "Sousse", latitude: 35.22, longitude: -101.83);
  //final _origin = Location(name: "City Hall", latitude: 42.886448, longitude: -78.878372);
  final _destination = Location(
      name: "Monastir", latitude: 35.21, longitude: -101.83);
        
  
  @override
  void initState() {

    super.initState();
    Future.delayed(const Duration(seconds: 0),(){
      setState(() {
        id = ModalRoute.of(context).settings.arguments;
        //print('f wost el '+id);
      });
      getOneRestaurant(id).then((data) {
        setState(() {
          restaurant = data;
        });
      });
      getMenuByRestaurant().then((data){
        setState(() {
          menus = data;
        });
      });
      getCurrentLocation().then((position) {
        setState(() {
          currentPosition = position;
        });
      });
    });
   
    _directions = MapboxNavigation(onRouteProgress: (arrived) async {});
  }

  
  @override 
  Widget build (BuildContext context){
    MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold(
      backgroundColor: CupertinoColors.systemGrey6,
      body: SingleChildScrollView( 
          child:restaurant!=null?Column(
            children: <Widget>[
              //header ( image and title )
              Container(
                height: 260,
                width: MediaQuery.of(context).size.width,
                child:Stack(
                  children:<Widget>[ 
                   ShaderMask(
                      shaderCallback: (rect) {
                        return LinearGradient(
                          begin: Alignment.center,
                          end: Alignment.bottomCenter,
                          colors: [Colors.black, Colors.transparent],
                        ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
                      },
                      blendMode: BlendMode.dstIn,
                      child:restaurant['restaurant_image']!=null? Container(
                        height: 240,
                        decoration: BoxDecoration(
                          image:DecorationImage(image: NetworkImage(url+'5011/images/'+restaurant['restaurant_image']),
                            fit: BoxFit.fill,
                          ),
                          borderRadius: BorderRadius.only(bottomRight:Radius.circular(100.0)),
                        ),
                      ):Container(),
                    
                    ),
                    
                    Align(
                      alignment: Alignment(-0.85,-0.65),
                      child:Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          color:Colors.white,
                        ),
                        child:IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          color: Colors.black,
                          iconSize: 20.0,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment(0.9,0.8),
                      child:Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          color:Colors.white,
                        ),
                        child:Icon(
                          Icons.location_on,
                          color: Colors.black,
                          size:20,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        '${restaurant['name']}',
                        style: TextStyle(
                          fontSize: 38,
                          color:colorCustom,
                          fontFamily: 'Montserrat',
                          shadows: [
                            Shadow(
                              blurRadius: 1.0,
                              color: Colors.grey,
                              offset: Offset(3.0, 3.0),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    
                    Container(
                      padding: EdgeInsets.only(left:5),
                      child: Text(
                        'Menu',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize:22,
                          fontWeight:FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                    CupertinoButton(
                      child: Text('See All >',
                      overflow: TextOverflow.ellipsis,
                        style:TextStyle(
                          fontFamily: 'Montserrat',
                        ),
                      ), 
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) =>Menu(),
                          settings: RouteSettings(arguments: restaurant['_id']),
                          )
                        ); 
                      },
                      //padding: EdgeInsets.fromLTRB(265, 0, 0, 0.0),
                    ),
                  ],
                ),
              ),
              //Horizontal listview
              Container(
                //margin: EdgeInsets.only(left:10),
                height: 280.0,
                child: (menus!=null)
                ? ListView.builder(
                    scrollDirection: Axis.horizontal,
                      itemCount: menus.length,
                      itemBuilder: (BuildContext context, int index) {
                        return  
                        Card(
                      child: InkWell(
                        splashColor: Colors.blue.withAlpha(30),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(
                                builder: (context) => MenuDetails(),
                                settings: RouteSettings(arguments: menus[index]['_id']),
                                )
                              );
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),
                          width: MediaQuery.of(context).size.width*0.8,
                          child:Stack(
                            children:<Widget>[
                              Container(
                                height: 280,
                                decoration: BoxDecoration(
                                  image:DecorationImage(image: NetworkImage(url+'5012/images/'+menus[0]['image']),
                                    fit: BoxFit.fill,
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                ),
                              ),
                              Align(
                                alignment: Alignment(0,0.85),
                                child: Container(
                                  padding: EdgeInsets.only(left:20,top:12,right:12,bottom: 12),
                                  width: MediaQuery.of(context).size.width*0.7,
                                  height: 90,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                    color:Colors.white,
                                  ),
                                  child:Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 8,
                                            child: Text(
                                              '${menus[index]['name']}',
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'Montserrat',
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: Text(
                                              '${menus[index]['price']}TND',
                                              style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'Montserrat',
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      
                                      Padding(padding: EdgeInsets.only(bottom: 5)),
                                      Divider(
                                        color: Colors.black,
                                        endIndent: 10,
                                        thickness: 0,
                                        
                                      ),
                                      Row(
                                          children: <Widget>[
                                            Container(
                                              width: MediaQuery.of(context).size.width*0.08,
                                              child: Icon(Icons.star,color:Colors.yellow),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width*0.15,
                                              child: Text(
                                                '4,9 (348)',
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                ),
                                              ),
                                            ),
                                            VerticalDivider(),
                                            Container(
                                              width: MediaQuery.of(context).size.width*0.08,
                                              child: Icon(Icons.timer),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width*0.15,
                                              child: Text(
                                                '${menus[index]['preparation_time']} min',
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ),
                      
                      ),
                    );
                  
                  })
                    : Center(
                      child: Text('nothing'),
                    ),
                    
                  
                
              ),
              SizedBox(
                height: 15,
              ),
              //create menu button
              CupertinoButton(
                child: Text(
                  'Create your meal',
                  style: TextStyle(
                   // fontSize:18,
                   color: Colors.black87,
                   fontWeight: FontWeight.bold,
                   fontFamily: 'Montserrat',
                  ),
                ), 
                color:colorCustom,
                borderRadius: new BorderRadius.circular(50.0),
                padding: EdgeInsets.fromLTRB(80.0, 0.0, 80.0, 0.0),
                onPressed: (){
                  Navigator.push(context, CupertinoPageRoute(builder: (context) => CustomizeMenu()));
                }
              ),
              SizedBox(
                height:15
              ),
              //working time week days
              Container(
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.bottomLeft,
              child: Text(
                  "Monday - Friday : ${restaurant['opens_at']} - ${restaurant['closes_at']}",
                  style: TextStyle(
                    fontWeight:FontWeight.bold,
                    fontSize: 16,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
              SizedBox(
                height: 2,
              ),
              Container(
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.bottomLeft,
              child: Text(
                  "Weekend : ${restaurant['opens_at']} - ${restaurant['closes_at']}",
                  style: TextStyle(
                    fontWeight:FontWeight.bold,
                    fontSize: 16,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.bottomLeft,
                margin: EdgeInsets.only(left:15),
                child: Text(
                  "Description :",
                  style: TextStyle(
                    fontSize: 26,
                    color: colorCustom,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(left:15),
                child:Text(
                        "${restaurant['description']}",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontFamily: 'Montserrat',
                        ),
                      ),
              ), 
              SizedBox(
                height: 10,
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 12,
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(0, 0.0, 0, 0.0),
                    child:StarRating(
                      rating: double.parse(restaurant['reviews']),
                      //onRatingChanged: (rating) => setState(() => this.restaurant['reviews'] = rating),
                    ),
                    width: 150,
                  ),
                  
                  Text(
                    "${restaurant['reviews']}",
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                    ),
                  ), 
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(12.0, 0.0, 15.0, 0.0),
                    child:Icon(Icons.call, color:Colors.black, size: 30,),
                    width: 50,
                    decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(70)),
                          border: Border.all(color: Colors.black) 
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text(
                    "${restaurant['phone_number']}",
                    style: TextStyle(
                      fontWeight:FontWeight.bold,
                      fontSize: 16,
                      fontFamily: 'Montserrat',
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                          border: Border.all(color: Colors.black) 
                ),
                width:450,
                height:250,
                child:FlutterMap(
                  options: MapOptions(
                      center: LatLng(double.parse(restaurant['location']['latitude']), double.parse(restaurant['location']['longitude'])), minZoom: 5, zoom: 15),
                  
                  layers: [
                    TileLayerOptions(
                      urlTemplate:
                          "https://api.mapbox.com/styles/v1/landolsii123/ck882htit03pn1iphovobfy6l/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibGFuZG9sc2lpMTIzIiwiYSI6ImNrN3p6NmQzcjAwaHozZW9kdXlqbjdyMXkifQ.Qn-dRKqwv7QEO-Mt8HtuRw",
                      additionalOptions: {
                          'accessToken':'pk.eyJ1IjoibGFuZG9sc2lpMTIzIiwiYSI6ImNrN3p6NmQzcjAwaHozZW9kdXlqbjdyMXkifQ.Qn-dRKqwv7QEO-Mt8HtuRw',
                          'id': 'mapbox.mapbox-streets-v7'
                      },
                    ),
                    MarkerLayerOptions(
                      markers: [
                        Marker(
                          width: 40,
                          height: 40,
                          point: LatLng(double.parse(restaurant['location']['latitude']), double.parse(restaurant['location']['longitude'])),
                          builder: (context) => new Container(
                                child: IconButton(
                                  icon: Icon(Icons.location_on),
                                  color: Colors.blue,
                                  iconSize: 45.0,
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) => CupertinoAlertDialog(
                                        title: new Text("Wanna Get there ?"),
                                        actions: <Widget>[
                                          CupertinoDialogAction(
                                            isDefaultAction: true,
                                            child: Text("Let's go"),
                                            onPressed: ()  async{
                                              await _directions.startNavigation(
                                                origin: _origin,
                                                destination: _destination,
                                                mode: NavigationMode.drivingWithTraffic,
                                                simulateRoute: true, language: "German", units: VoiceUnits.metric
                                              );
                                            },
                                          ),
                                          CupertinoDialogAction(
                                            child: Text("no, It's okay",style: TextStyle(color:Colors.red),),
                                            onPressed: (){
                                              Navigator.of(context, rootNavigator: true).pop();
                                            },
                                          )
                                        ],
                                      )
                                    );
                                  },
                                ),
                              ),
                        ),
                      ],
                    ),
                    
                  ],
                ),
              )
            ],
            
          ):CircularProgressIndicator(),
      ), 
      );
  }
}



typedef void RatingChangeCallback(double rating);

class StarRating extends StatelessWidget {
  final int starCount;
  final double rating;
  final RatingChangeCallback onRatingChanged;
  final Color color;

  StarRating({this.starCount = 5, this.rating = .0, this.onRatingChanged, this.color});

  Widget buildStar(BuildContext context, int index) {
    Icon icon;
    if (index >= rating) {
      icon = new Icon(
        Icons.star_border,
        color: Theme.of(context).buttonColor,
      );
    }
    else if (index > rating - 1 && index < rating) {
      icon = new Icon(
        Icons.star_half,
        color: Colors.yellow,
        size: 30,
      );
    } else {
      icon = new Icon(
        Icons.star,
        color: Colors.yellow,
        size: 30,
      );
    }
    return new InkResponse(
      onTap: onRatingChanged == null ? null : () => onRatingChanged(index + 1.0),
      child: icon,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Row(children: new List.generate(starCount, (index) => buildStar(context, index)));
  }
}


