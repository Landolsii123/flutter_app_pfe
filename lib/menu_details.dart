import 'package:flutter/cupertino.dart' ;
import 'package:flutter/material.dart';
import 'package:healthy_food_locator/http_service.dart';
import 'styles.dart';


class MenuDetails extends StatefulWidget {
  @override
  _MenuDetails createState() => _MenuDetails();
}

class _MenuDetails extends State<MenuDetails>{

  var menuDetails;
  var id;
  var ingredients='';
  //Position currentPosition;
  

  @override
  void initState() {

    super.initState();
   Future.delayed(const Duration(seconds: 0),(){
      setState(() {
        id = ModalRoute.of(context).settings.arguments;
        //print('f wost el '+id);
      });
      getMenuDetails(id).then((data) {
        for(int i=0;i<data['ingredients'].length;i++){
          ingredients = '$ingredients ${data['ingredients'][i]['name']}, ';
        }
        setState(() {
          menuDetails = data;

        });
        
        
      });
        
        
        
      
    });

    
  }

  @override 
  Widget build (BuildContext context){
    MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold(
      backgroundColor: CupertinoColors.systemGrey6,
      body: SingleChildScrollView(
        child:
        menuDetails!=null?
        Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 340,
                width: MediaQuery.of(context).size.width,
                child:Stack(
                  children:<Widget>[ 
                    Container(
                      height: 300,
                      decoration: BoxDecoration(
                        image:DecorationImage(image: NetworkImage(url+'5012/images/'+menuDetails['image']),
                          fit: BoxFit.fill,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                    ),
                    
                    Align(
                      alignment: Alignment(0.55,-0.75),
                      child:Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          color:Colors.white,
                        ),
                        child:Icon(
                          Icons.favorite_border,
                          color: Colors.black,
                          size:20
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment(0.85,-0.75),
                      child:Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          color:Colors.white,
                        ),
                        child:Icon(
                          Icons.shopping_cart,
                          color: Colors.black,
                          size:20
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment(-0.85,-0.75),
                      child:Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          color:Colors.white,
                        ),
                        child:IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          color: Colors.black,
                          iconSize: 20.0,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ),
                    
                    Align(
                      alignment: Alignment.bottomCenter,
                      child:Container(
                        padding: EdgeInsets.only(left:20,top:12,right:12,bottom: 12),
                        width: MediaQuery.of(context).size.width*0.8,
                        height: 120,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          color:Colors.white,
                        ),
                        child:Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              '${menuDetails['name']}',
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat'
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top:3)
                            ),
                            Text(
                              '${menuDetails['categories']}',
                              style: TextStyle(
                                fontSize: 14,
                                //fontWeight: FontWeight.bold,
                                color: Colors.grey,
                                fontFamily: 'Montserrat'
                              ),
                            ),
                            
                            Padding(padding: EdgeInsets.only(bottom: 12)),

                            Divider(
                              color: Colors.black,
                              endIndent: 10,
                              thickness: 0,
                              
                            ),
                            Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width*0.07,
                                    child: Icon(Icons.star,color:Colors.yellow),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width*0.16,
                                    child: Text(
                                      ' 4,9 (348)',
                                      
                                      style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  VerticalDivider(),
                                  Container(
                                    width: MediaQuery.of(context).size.width*0.07,
                                    child: Icon(Icons.timer),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width*0.15,
                                    child: Text(
                                      '${menuDetails['preparation_time']} min',
                                      
                                      style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  VerticalDivider(),
                                  
                                  Container(
                                    width: MediaQuery.of(context).size.width*0.15,
                                    child: Text(
                                      
                                      '${menuDetails['price']} TND',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.bold,
                                        
                                      ),
                                    ),
                                  ),
                                  
                                ],
                              ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ), 
              Padding(padding: EdgeInsets.only(top:20)),
              Container(
                padding: EdgeInsets.only(left:10,right:10),
                child:Text(
                  'Description :',
                  style:TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat'
                  ),
                ),   
              ),
              Container(
                padding: EdgeInsets.only(left:10,top:10,right:10),
                child:Text(
                  '${menuDetails['description']}',
                  style:TextStyle(
                    fontSize: 17,
                    fontStyle: FontStyle.italic,
                    fontFamily: 'Montserrat'
                  ),
                ),   
              ),
              Padding(padding: EdgeInsets.only(top:20)),
              Container(
                padding: EdgeInsets.only(left:10,right:10),
                child:Text(
                  'Ingredients :',
                  style:TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat'
                  ),
                ),   
              ),
              Container(
                padding: EdgeInsets.only(left:10,top:10,right:10),
                child:Text(
                  "$ingredients",
                  style:TextStyle(
                    fontSize: 17,
                    fontStyle: FontStyle.italic,
                    fontFamily: 'Montserrat'
                  ),
                ),   
              ),
            ],
          ):
          CircularProgressIndicator(),
      ), 
    );
  }
}


/*
typedef void RatingChangeCallback(double rating);

class StarRating extends StatelessWidget {
  final int starCount;
  final double rating;
  final RatingChangeCallback onRatingChanged;
  final Color color;

  StarRating({this.starCount = 5, this.rating = .0, this.onRatingChanged, this.color});

  Widget buildStar(BuildContext context, int index) {
    Icon icon;
    if (index >= rating) {
      icon = new Icon(
        Icons.star_border,
        color: Theme.of(context).buttonColor,
      );
    }
    else if (index > rating - 1 && index < rating) {
      icon = new Icon(
        Icons.star_half,
        color: Colors.yellow,
        size: 30,
      );
    } else {
      icon = new Icon(
        Icons.star,
        color: Colors.yellow,
        size: 30,
      );
    }
    return new InkResponse(
      onTap: onRatingChanged == null ? null : () => onRatingChanged(index + 1.0),
      child: icon,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Row(children: new List.generate(starCount, (index) => buildStar(context, index)));
  }
}

*/

/*Stack(
  children:<Widget>[ 
    /*ClipRRect(
      borderRadius: BorderRadius.only(bottomRight:Radius.circular(100.0)),
      child:Image(
        image: AssetImage('assets/resto.jpg'),
      ),
    ),*/
    Container(
      width: MediaQuery.of(context).size.width,
      height: 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(bottomRight:Radius.circular(50.0),bottomLeft:Radius.circular(50.0)),
        color: colorCustom,
      ),
    ),
    Align(
      alignment: Alignment(0.55,-0.75),
      child:Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(50.0)),
          color:Colors.white,
        ),
        child:Icon(
          Icons.favorite_border,
          color: Colors.black,
          size:20
        ),
      ),
    ),
    Align(
      alignment: Alignment(0.85,-0.75),
      child:Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(50.0)),
          color:Colors.white,
        ),
        child:Icon(
          Icons.shopping_cart,
          color: Colors.black,
          size:20
        ),
      ),
    ),
    Align(
      alignment: Alignment(-0.85,-0.75),
      child:Text(
        'Sandwish Thon',
        style: TextStyle(
          fontSize:20,
          color:Colors.black,
          fontWeight: FontWeight.bold
        ),
      ),
    ),
    Align(
      alignment: Alignment.center,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        child:Image(
          image: AssetImage('assets/sandwish.jpg'),
          width: MediaQuery.of(context).size.width*0.9,
        ),
      ),
    ),
    Align(
      alignment: Alignment(0,0.35),
      child:Container(
        padding: EdgeInsets.only(left:20,top:12,right:12,bottom: 12),
        width: MediaQuery.of(context).size.width*0.8,
        height: 120,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color:Colors.white,
        ),
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Pizzaria Pechtri',
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold
              ),
            ),
        
            Text(
              'Gour',
              style: TextStyle(
                fontSize: 14,
                //fontWeight: FontWeight.bold,
                color: Colors.grey
              ),
            ),
            
            Padding(padding: EdgeInsets.only(bottom: 12)),

            Divider(
              color: Colors.black,
              endIndent: 10,
              thickness: 0,
              
            ),
            Row(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width*0.08,
                    child: Icon(Icons.star,color:Colors.yellow),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width*0.15,
                    child: Text(' 4,9 (348)'),
                  ),
                  VerticalDivider(),
                  Container(
                    width: MediaQuery.of(context).size.width*0.08,
                    child: Icon(Icons.timer),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width*0.15,
                    child: Text(' 20-30 min'),
                  ),
                  VerticalDivider(),
                  Container(
                    width: MediaQuery.of(context).size.width*0.08,
                    child: Icon(Icons.euro_symbol),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width*0.1,
                    child: Text(' 200'),
                  ),
                  //Icon(Icons.star,color:Colors.yellow),
                  //Text(' 4,9 (348)'),
                  //Padding(padding: EdgeInsets.only(left:5)),
                  //VerticalDivider(),
                  //Icon(Icons.timer),
                  //Text(' 20-30 min'),
                  //VerticalDivider(),
                  //Icon(Icons.euro_symbol)
                  //Text(' 200'),
                ],
              ),
          ],
        ),
      ),
    ),
  ],
),
*/








/*
Stack(
  children:<Widget>[ 
    ClipRRect(
      borderRadius: BorderRadius.only(bottomRight:Radius.circular(50.0),bottomLeft:Radius.circular(50.0)),
      child:Image(
        image: AssetImage('assets/sandwish.jpg'),
      ),
    ),
    
    Align(
      alignment: Alignment(0.55,-0.75),
      child:Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(50.0)),
          color:Colors.white,
        ),
        child:Icon(
          Icons.favorite_border,
          color: Colors.black,
          size:20
        ),
      ),
    ),
    Align(
      alignment: Alignment(0.85,-0.75),
      child:Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(50.0)),
          color:Colors.white,
        ),
        child:Icon(
          Icons.shopping_cart,
          color: Colors.black,
          size:20
        ),
      ),
    ),
    Align(
      alignment: Alignment(-0.85,-0.75),
      child:Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(50.0)),
          color:Colors.white,
        ),
        child:Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
          size:20
        ),
      ),
    ),
    
    Align(
      alignment: Alignment.bottomCenter,
      child:Container(
        padding: EdgeInsets.only(left:20,top:12,right:12,bottom: 12),
        width: MediaQuery.of(context).size.width*0.8,
        height: 120,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color:Colors.white,
        ),
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Sandish Thon',
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top:3)
            ),
            Text(
              'Gour',
              style: TextStyle(
                fontSize: 14,
                //fontWeight: FontWeight.bold,
                color: Colors.grey
              ),
            ),
            
            Padding(padding: EdgeInsets.only(bottom: 12)),

            Divider(
              color: Colors.black,
              endIndent: 10,
              thickness: 0,
              
            ),
            Row(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width*0.08,
                    child: Icon(Icons.star,color:Colors.yellow),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width*0.15,
                    child: Text(' 4,9 (348)'),
                  ),
                  VerticalDivider(),
                  Container(
                    width: MediaQuery.of(context).size.width*0.08,
                    child: Icon(Icons.timer),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width*0.15,
                    child: Text(' 30 min'),
                  ),
                  VerticalDivider(),
                  Container(
                    width: MediaQuery.of(context).size.width*0.08,
                    child: Icon(Icons.euro_symbol),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width*0.1,
                    child: Text(' 200'),
                  ),
                  //Icon(Icons.star,color:Colors.yellow),
                  //Text(' 4,9 (348)'),
                  //Padding(padding: EdgeInsets.only(left:5)),
                  //VerticalDivider(),
                  //Icon(Icons.timer),
                  //Text(' 20-30 min'),
                  //VerticalDivider(),
                  //Icon(Icons.euro_symbol)
                  //Text(' 200'),
                ],
              ),
          ],
        ),
      ),
    ),
  ],
),
              
*/ 