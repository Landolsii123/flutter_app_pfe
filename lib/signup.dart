import 'package:flutter/cupertino.dart' ;
import 'package:flutter/material.dart';
import 'styles.dart';
import 'login.dart';



class SignUp extends StatelessWidget{
  @override 
  Widget build (BuildContext context){
    MaterialColor colorCustom = MaterialColor(0xFF12a19a, color);
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Register')
      ),
      child:Container(
      child:Center(
        child: SingleChildScrollView(
          child:Column(
            children: <Widget>[
              SizedBox(
                height:50.0,
              ),
              CupertinoTextField(
                placeholder: 'Firstname..',
                padding: EdgeInsets.all(16.0),
              ),
              SizedBox(
                height:5.0
              ),
              CupertinoTextField(
                placeholder: 'Lastname..',
                padding: EdgeInsets.all(16.0),
              ),
              SizedBox(
                height:5.0
              ),
              CupertinoTextField(
                placeholder: 'Birthday..',
                padding: EdgeInsets.all(16.0),
              ),
              SizedBox(
                height:5.0
              ),
              CupertinoTextField(
                placeholder: 'Gender..',
                padding: EdgeInsets.all(16.0),
              ),
              SizedBox(
                height:5.0
              ),
              CupertinoTextField(
                placeholder: 'Email..',
                padding: EdgeInsets.all(16.0),
              ),
              SizedBox(
                height:5.0
              ),
              CupertinoTextField(
                placeholder: 'Password..',
                padding: EdgeInsets.all(16.0),
              ),
              SizedBox(
                height:5.0
              ),
              CupertinoTextField(
                placeholder: 'Phone number..',
                padding: EdgeInsets.all(16.0),
              ),
              SizedBox(
                height: 25.0,
              ),
              CupertinoButton(
                child: Text('OOOO'),
                onPressed: (){},
                color: colorCustom,
                borderRadius: new BorderRadius.circular(50.0),
                padding: EdgeInsets.fromLTRB(100.0, 0.0, 100.0, 0.0),
              ),
              SizedBox(
                height:15.0,
              ),
              Text(
                'Already have an account ?'
              ),
              CupertinoButton(
                child: Text('Sign In'),
                onPressed: (){Navigator.push(context, CupertinoPageRoute(builder: (context) => Login()));},
              ),
            ],
          ),
        ),
      ),
      ),
    );
  }
}