import 'package:flutter/cupertino.dart' ;
import 'package:flutter/material.dart';
import 'package:healthy_food_locator/user_profile.dart';
import 'styles.dart';
import 'http_service.dart';

class AddMeasures extends StatefulWidget {
  @override
  _AddMeasures createState() => _AddMeasures();
}

class _AddMeasures extends State<AddMeasures>{
  bool loader=false;
  final weight=TextEditingController();
  final height=TextEditingController();
  final neck=TextEditingController();
  final waist=TextEditingController();
  final hip=TextEditingController();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed. 
    weight.dispose();
    height.dispose();
    neck.dispose();
    waist.dispose();
    hip.dispose();
    super.dispose();
  }

  @override 
  Widget build (BuildContext context){
    MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold(
      body:Center(
          child: SingleChildScrollView(
            child:Container(
              width:MediaQuery.of(context).size.width*0.6,
            child:Column(
              children: <Widget>[
                Text(
                  'Measures',
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat'
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width*0.25,
                  margin: EdgeInsets.all(10),
                  child:CupertinoTextField(
                    controller: height,
                    placeholder: 'Height..',
                    padding: EdgeInsets.all(16.0),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width*0.25,
                  margin: EdgeInsets.all(10),
                  child:CupertinoTextField(
                    controller: weight,
                    placeholder: 'Weight..',
                    padding: EdgeInsets.all(16.0),
                  ),
                ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child:CupertinoTextField(
                    controller: neck,
                    placeholder: 'Neck..',
                    padding: EdgeInsets.all(16.0),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child:CupertinoTextField(
                    controller: waist,
                    placeholder: 'Waist..',
                    padding: EdgeInsets.all(16.0),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child:CupertinoTextField(
                    controller: hip,
                    placeholder: 'Hip..',
                    padding: EdgeInsets.all(16.0),
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                loader?CircularProgressIndicator():
                CupertinoButton(
                  child: Text(
                    'Add My Measures',
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: () async{
                    setState(() {
                      loader=true;
                    });
                    print(weight.text);
                    await addMeasures(ModalRoute.of(context).settings.arguments,height.text,weight.text,neck.text,waist.text,hip.text);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Profile()));

                    setState(() {
                    loader=false;
                    });
                    
                  },
                  color: colorCustom,
                  borderRadius: new BorderRadius.circular(50.0),
                  padding: EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 0.0),
                ),
              ],
            ),
          ),
          ),
        ),
    );
  }
}