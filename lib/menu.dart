import 'package:flutter/cupertino.dart' ;
import 'package:flutter/material.dart';
import 'package:healthy_food_locator/menu_details.dart';
import 'styles.dart';
import 'http_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class Menu extends StatefulWidget {
  @override
  _Menu createState() => _Menu();
}

class _Menu extends State<Menu>{

  var menus = {};
  var cart =[];

  descriptionMaker(menu){
    String decription ="";
    for (int i = 0 ; i < menu['ingredients'].length ; i++){
      decription+=menu['ingredients'][i]['name']+" ";
    }
    return decription;
  }

  caloriesCalculator(menu){
    double sum = 0;
    for (int i = 0 ; i < menu['ingredients'].length ; i++){
      sum+=double.parse(menu['ingredients'][i]['weight']);
    }
    return sum.toStringAsFixed(0);
  }

  getResto(resto){
    print('aaaaaaaaaaaaaaaaaaaaaaaa');
    print(resto[0]['_id']);
    //return resto['address'];
  }

  void initState() {
    getMenuByRestaurant().then((data) {
      setState(() {
        for(int i=0; i<data.length ;i++)
        menus[i] = data[i];
        
      });
    });
    super.initState();
  }

  addtoCart(menu) async{
    var oldCart;
    
    
      final prefs = await SharedPreferences.getInstance();
      if(prefs.getString('cart')==null ){
        cart.add(menu);
        await prefs.setString('cart', json.encode(cart));
        print('first item');
        print(json.decode(prefs.getString('cart')));
      }
      else{
        oldCart= json.decode(prefs.getString('cart'));
        print(oldCart.length);
        oldCart.add(menu);
        prefs.remove('cart');
        await prefs.setString('cart', json.encode(oldCart));
        //print('hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh');
        //print(json.decode(prefs.getString('cart')).length);
        
      }
  }

  @override 
  Widget build (BuildContext context){
    MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold(
        body:SingleChildScrollView(
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[   
              SizedBox(
                height: 35,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left:10,right:10),
                      child: IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          color: Colors.black,
                          iconSize: 20.0,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                    ),
                  ),
                  Expanded(
                    flex: 6,
                    child: Text(
                      'Menus',
                      style: TextStyle(
                        fontSize: 22,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
                
              ),
              Container(
                margin: EdgeInsets.fromLTRB(5, 15, 5, 10),
                child: CupertinoTextField(
                  placeholder: 'Search ..',
                ),
              ),
              
              Container(
                height: 400,
                padding: EdgeInsets.all(10),
                  child: menus.length>0
                    ? ListView.builder(
                      itemCount: menus.length,
                      itemBuilder: (BuildContext context, int index) {
                        //print(menus[index]['restaurant']['_id']);
                        
                          return 
                          Card(
                            child: InkWell(
                              splashColor: Colors.blue.withAlpha(30),
                              onTap: () {
                                print(menus[index]['_id']);
                                Navigator.push(context, MaterialPageRoute(
                                builder: (context) => MenuDetails(),
                                settings: RouteSettings(arguments: menus[index]['_id']),
                                )
                              );
                              },
                              child: Container(
                                height: 100,
                                child:Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 3, 
                                      child: Container(
                                        decoration: BoxDecoration(
                                          image:DecorationImage(image: NetworkImage(url+'5012/images/'+menus[0]['image']),
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 7, 
                                      child: Column(
                                        children: <Widget>[
                                          Expanded(
                                            child:Row(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 9,
                                                  child: Container(
                                                    margin: EdgeInsets.only(left: 5),
                                                    child: Text(
                                                      '${menus[index]['name']}',
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize:18,
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: Container(
                                                    //margin: EdgeInsets.only(left: 1),
                                                    child: Text(
                                                      '${menus[index]['price']}TND',
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize:16,
                                                        color:Colors.black45,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child:Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 8,
                                                  child: Container(
                                                    //margin: EdgeInsets.only(left: 5,right: 5,top: 0),
                                                    padding: EdgeInsets.only(left: 5,right: 5,top: 0),
                                                    child:Text(
                                                      '${descriptionMaker(menus[index])}', 
                                                      style:TextStyle(
                                                        fontSize:14,
                                                        fontFamily: 'Montserrat'
                                                      )
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child:Row(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 4,
                                                  child: Container(
                                                    margin: EdgeInsets.only(left: 10),
                                                    child: Text(
                                                      '${menus[index]['preparation_time']}min',
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize:16,
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 4,
                                                  child: Container(
                                                    margin: EdgeInsets.only(left: 10),
                                                    child: Text(
                                                      '${caloriesCalculator(menus[index])}Cal',
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize:16,
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 3,
                                                  child: Container(
                                                    margin: EdgeInsets.only(left: 10),
                                                    child: IconButton(
                                                      icon: Icon(Icons.add_shopping_cart),
                                                      color: colorCustom,
                                                      iconSize: 25.0,
                                                      onPressed: () {
                                                        showDialog(
                                                          context: context,
                                                          builder: (BuildContext context) => CupertinoAlertDialog(
                                                            title: new Text("You sure wanna add this to cart ?"),
                                                            actions: <Widget>[
                                                              CupertinoDialogAction(
                                                                isDefaultAction: true,
                                                                child: Text("Yes !"),
                                                                onPressed: (){
                                                                  //print(menus[index]);
                                                                  addtoCart(menus[index]);
                                                                  //print('c boooooon menu');
                                                                  Navigator.of(context, rootNavigator: true).pop();
                                                                },
                                                              ),
                                                              CupertinoDialogAction(
                                                                child: Text("no, It's okay",style: TextStyle(color:Colors.red),),
                                                                onPressed: (){
                                                                  Navigator.of(context, rootNavigator: true).pop();
                                                                },
                                                              )
                                                            ],
                                                          )
                                                        );
                                                      },
                                                    )
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ),
                          );
                        
                        
                      })
                  : Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ],
          ),
        ),
    );
  }
}


/* 
  
              Card(
                child: InkWell(
                  splashColor: Colors.blue.withAlpha(30),
                  onTap: () {
                    print('Card tapped.');
                  },
                  child: Container(
                    height: 120,
                    child:Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3, 
                          child: Image(
                            image: NetworkImage(url+'5012/images/'+menus[0]['image']),
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        Expanded(
                          flex: 7, // 40% of space
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                child:Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 9,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 5),
                                        child: Text(
                                          '${menus[0]['name']}',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:18,
                                            color:CupertinoColors.systemGrey
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        //margin: EdgeInsets.only(left: 1),
                                        child: Text(
                                          '30€',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:16,
                                            color:Colors.black45
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child:Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 8,
                                      child: Container(
                                        //margin: EdgeInsets.only(left: 5,right: 5,top: 0),
                                        padding: EdgeInsets.only(left: 5,right: 5,top: 0),
                                        child:Text(
                                          '${descriptionMaker(menus)}', 
                                          style:TextStyle(
                                            fontSize:14,
                                            
                                          )
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child:Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 4,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          '35min',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:16,
                                            color:CupertinoColors.systemGrey
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          '${caloriesCalculator(menus)}Cal',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:16,
                                            color:CupertinoColors.systemGrey
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Icon(
                                          Icons.add_shopping_cart, 
                                          color:colorCustom,
                                          size: 30,
                                        ),
                                      ),
                                    ),
                                   ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ),
              ),
             
*/