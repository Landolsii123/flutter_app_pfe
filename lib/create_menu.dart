import 'dart:convert';
import 'package:healthy_food_locator/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'styles.dart';
import 'http_service.dart';

class CustomizeMenu extends StatefulWidget {
  @override
  _CustomizeMenu createState() => new _CustomizeMenu();
}

class _CustomizeMenu extends State<CustomizeMenu> {

var ingredients={} ;

var ing = [];

var menu={'name':'Created menu','restaurant_name':'','price':0, 'ingredients':[]};
var cart=[];
var menuArray=[];
var ingredient_types = {0:{'name':'Meat','clicked':'true'},1:{'name':'Vegetable','clicked':'false'},2:{'name':'Bread','clicked':'false'},3:{'name':'Drink','clicked':'false'},};

double menu_price = 0 ;

  addtoCart(menu) async{
      var oldCart=[];
      final prefs = await SharedPreferences.getInstance();
      if(prefs.getString('cart')==null ){
        cart.add(menu);
        await prefs.setString('cart', json.encode(cart));
      }
      else{
        oldCart= json.decode(prefs.getString('cart'));
        oldCart.add(menu);
        prefs.remove('cart');
        await prefs.setString('cart', json.encode(oldCart));
      }
  }

  void initState() {
    getAllIngredients().then((data) {
      setState(() {
        ///*clicks_counter*/=0;
        menu_price=0;
        for(int i=0;i<data.length;i++){
          ingredients[i]={...data[i],'quantity':0};
        }
      });
      print(ingredients);
    });
    super.initState();
  }

  getClickedIngredientType(){
    for(int i=0 ; i<ingredient_types.length; i++){
      if(ingredient_types[i]['clicked']=='true'){
        return(ingredient_types[i]['name']);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold(
    
    body: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 40,
        ),
        //menu Label
        Container(
          height: 35,
          margin: EdgeInsets.only(left:20,top:10,bottom: 5),
          child: Text(
            'Ingredients',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              fontFamily: 'Montserrat'
            ),
          ),
        ),
         //ingredients list
        Container(
          height: MediaQuery.of(context).size.height*0.15,
          child:(ingredients.length>0)
            ? ListView.builder(
            scrollDirection: Axis.horizontal,
              itemCount: ingredients.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  margin: EdgeInsets.only(left: 5,right: 5,top:5),
                  child: InkWell(
                    splashColor: Colors.blue.withAlpha(30),
                    borderRadius: BorderRadius.all(Radius.circular(120)),
                    onTap: () {
                      print('Card tapped.');
                      setState(() {
                        ingredients[index]['quantity']++;
                        /*clicks_counter*/
                        menu_price=menu_price+double.parse(ingredients[index]['price']);
                      });
                    },
                    child:Column(
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(
                            color:Colors.white,
                            image:DecorationImage(image: NetworkImage(url+'5013/images/'+ingredients[index]['ingredient_image']),
                              fit: BoxFit.fill,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(120)),
                            border: ingredients[index]['quantity']>0?Border.all(
                              color: colorCustom,
                              width: 2
                            ):
                            Border.all(
                              color: Colors.black,
                              width: 1
                            )
                          ),
                        ),
                        
                        Container(
                          margin: EdgeInsets.only(top:5),
                          alignment: Alignment.center,
                          width: 80,
                          child: Text(
                            '${ingredients[index]['name']}',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontWeight:FontWeight.bold,
                              fontSize: 16,
                              fontFamily: 'Montserrat',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              })
            : Center(
              child: SizedBox(),
            ), 
          ),
         //category list
        Container(
          //padding: EdgeInsets.only(top:15),
          color: CupertinoColors.systemGrey6,
          height: 52,
          width:MediaQuery.of(context).size.width,
          child:ingredient_types.length>0?
            ListView.builder(
            scrollDirection: Axis.horizontal,
              itemCount: ingredient_types.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: 40,
                  //alignment: FractionalOffset.center,
                  margin: EdgeInsets.all(5),
                  //padding: EdgeInsets.all(5),
                  child: InkWell(
                    splashColor: Colors.blue.withAlpha(30),
                    borderRadius: BorderRadius.all(Radius.circular(120)),
                    onTap: () {
                      print('Card tapped.');
                      print(ingredient_types[index]['name']);
                      setState(() {
                        ingredient_types[index]['clicked']='true';
                      });
                      for(int i=0;i<ingredient_types.length;i++){
                        if(i != index && ingredient_types[index]['clicked']=='true'){
                          setState(() {
                            ingredient_types[i]['clicked']='false';
                          });
                        }
                      }
                    },
                    child:ingredient_types[index]['clicked']=='true'?
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        border:Border(
                          bottom: BorderSide(
                            width: 3,
                            color:colorCustom,
                          ),
                        ) 
                      ),
                      child: Text(
                        '${ingredient_types[index]['name']}',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ):Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        
                        '${ingredient_types[index]['name']}',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ),
                );
              })
            : Center(
              child: SizedBox(),
            ), 
        ),
        //Menu ( invoice )
        Container(
          //padding: EdgeInsets.only(top:5),
          color: CupertinoColors.systemGrey6,
          height:MediaQuery.of(context).size.height*0.554,
          child:ingredients.length>0
            ? ListView.builder(
            scrollDirection: Axis.vertical,
              itemCount: ingredients.length,
              itemBuilder: (BuildContext context, int index) {
                return ( (ingredients[index]['type']==getClickedIngredientType()))? Container(
                  margin: EdgeInsets.all(5),
                  child: InkWell(
                    splashColor: Colors.blue.withAlpha(30),
                    //borderRadius: BorderRadius.all(Radius.circular(20)),
                    onTap: () {
                      print('Card tapped.');
                      setState(() {
                        /*clicks_counter*/
                        menu_price=menu_price - (double.parse(ingredients[index]['price'])*ingredients[index]['quantity']);
                        ingredients[index]['quantity']=0;
                      });
                      
                    },
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            //picture
                            Container(
                              margin: EdgeInsets.only(left:5,right: 20),
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                image:DecorationImage(image: NetworkImage(url+'5013/images/'+ingredients[index]['ingredient_image']),
                                  fit: BoxFit.fill,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(120)),
                                border:  ingredients[index]['quantity']>0?Border.all(
                                  color: colorCustom,
                                  width: 2
                                ):
                                Border.all(
                                  color: Colors.black,
                                  width: 1
                                )
                              ),
                            ),
                            //ingredient name and price
                            Container(
                              padding: EdgeInsets.only(right:10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        '${ingredients[index]['name']}',
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontWeight:FontWeight.bold,
                                          fontSize: 16,
                                          fontFamily: 'Montserrat',
                                        ),
                                      ),
                                      Text(
                                        ingredients[index]['type']=="Drink"?
                                        '(${ingredients[index]['weight']}ml)':
                                        '(${ingredients[index]['weight']}g)',
                                        style: TextStyle(
                                          fontWeight:FontWeight.bold,
                                          fontSize: 12,
                                          fontFamily: 'Montserrat',
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(right:10),
                                        child: Text(
                                          '${ingredients[index]['price']} TND',
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: 'Montserrat',
                                            color: colorCustom,
                                            fontWeight: FontWeight.bold
                                          ),
                                        ),
                                      ),
                                      Text(
                                        '${ingredients[index]['calories']} Cal',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: 'Montserrat',
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                        
                          ],
                        ),
                        //add remove button
                        Container(
                          padding: EdgeInsets.only(left:10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                decoration:BoxDecoration(
                                  color:CupertinoColors.systemGrey4,
                                  borderRadius: BorderRadius.all(Radius.circular(10))
                                ),
                                width: 40,
                                height: 40,
                                child: IconButton(
                                  icon: Icon(Icons.remove),
                                  iconSize: 20,
                                  color: Colors.black,
                                  onPressed: () {
                                    if(ingredients[index]['quantity']==0){
                                      setState(() {
                                        
                                        //menu_price=menu_price - double.parse(ingredients[index]['price']);
                                        ingredients[index]['quantity']=0;
                                        /*clicks_counter*/
                                      });
                                    }
                                    else{
                                      setState(() {
                                        ingredients[index]['quantity']--;
                                        menu_price=menu_price - double.parse(ingredients[index]['price']);
                                      });
                                    }
                                  },
                                ),
                              ), 
                              SizedBox(width:10),
                              Text(
                                "${ingredients[index]['quantity']}",
                                style:TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 16
                                ),
                              ),
                              SizedBox(width: 10),
                              Container(
                                width: 40,
                                height: 40,
                                decoration:BoxDecoration(
                                  color:CupertinoColors.systemGrey4,
                                  borderRadius: BorderRadius.all(Radius.circular(10))
                                ),
                                child: IconButton(
                                  icon: Icon(Icons.add),
                                  iconSize: 20,
                                  color: Colors.black,
                                  onPressed: () {
                                    setState(() {
                                      ingredients[index]['quantity']++;
                                      menu_price=menu_price + double.parse(ingredients[index]['price']);
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ):
                SizedBox();
              })
            : Center(
              child: Text('nothing'),
            ), 
          ),
        //add to cart button
        Container(
          color: CupertinoColors.systemGrey6,
          padding: EdgeInsets.only(bottom:10),
          child: Container(
          color: CupertinoColors.systemGrey6,
            child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              borderRadius: BorderRadius.all(Radius.circular(120)),
              onTap: () async{
                SharedPreferences prefs = await SharedPreferences.getInstance();
               
                //print('aaaaaaaaaaaaa'+prefs.getString('cart'));
                //prefs.clear();
                if(menu_price==0){
                  print('Button tapped.');
                }
                else{
                  
                  showDialog(
                  context: context,
                  builder: (BuildContext context) => CupertinoAlertDialog(
                    title: new Text("You sure wanna add this to cart ?"),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        isDefaultAction: true,
                        child: Text("Yes !"),
                        onPressed: (){
                          int j=0;
                          for(int i=0;i<ingredients.length;i++){
                            if(ingredients[i]['quantity']>0){
                              ing.add(ingredients[i]);
                              
                            }
                          }
                          menu['restaurant_name']='Not Specified';
                          menu['price']=menu_price.toString();
                          menu['ingredients']=ing;
                          addtoCart(menu);
                          print(menu);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                        },
                      ),
                      CupertinoDialogAction(
                        child: Text("no, It's okay",style: TextStyle(color:Colors.red),),
                        onPressed: (){
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                      )
                    ],
                  )
                );
                
                }
              },
              child:Container(
                color: colorCustom,
                width: MediaQuery.of(context).size.width*0.8,
                height: 50,
                child: Row(

                  children: <Widget>[
                    Expanded(
                      flex: 8,
                      child: Container(
                        alignment:  FractionalOffset(0.7, 0.5),
                        child: Text(
                          'Add to cart',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                            fontSize: 18
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child:menu_price==0? Text(
                        '${menu_price.toStringAsFixed(1)} TND',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                          fontSize: 18
                        ),
                      ):
                      Text(
                        '${menu_price.toStringAsFixed(1)} TND ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                          fontSize: 18
                        ),
                      )
                    ),
                  ],
                ),
              )
            ),
          ),
        ),
       

        )
        ],
      ),
    );
  }
  
}


class Ingre {
  String id;
  String name;
  String weight;
  String type;
  String calories;
  String price;
  String quantity;

    Ingre({this.id,this.name,this.weight,this.type,this.calories,this.price});
}