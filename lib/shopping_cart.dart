import 'package:flutter/cupertino.dart' ;
import 'package:flutter/material.dart';
import 'styles.dart';
import 'http_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class ShoppingCart extends StatefulWidget {
  @override
  _ShoppingCart createState() => _ShoppingCart();
}

class _ShoppingCart extends State<ShoppingCart>{

  var cart=[];
  var price=0.0;

  displayCart() async{
    final prefs = await SharedPreferences.getInstance();
    if(prefs.getString('cart')!=null){
      return json.decode(prefs.getString('cart'));
    }
    else{
      return null;
    }
  }
  
  @override
  void initState() {
    super.initState();
    displayCart().then(

      (data){
        setState(() {
          cart=data;
          for(int i=0;i<data.length;i++){
            cart[i]['quantity']=1;
            price=price+double.parse(cart[i]['price']);
          }
          //print(cart[0]['price']);
          print("baaaa");
          print(cart.length);
        });
      }
    );
  }

  descriptionMaker(ings){
    var result='';
    //print(ings[2]);
    for(int i=0;i<ings.length;i++){
      result = '$result ${ings[i]['name']}, ';
    }
    return result;
  }

  
  @override 
  Widget build (BuildContext context){
    MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return CupertinoPageScaffold(
      child:SingleChildScrollView(
       child:(cart!=null)&&(cart.length>0)?
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[   
            SizedBox(
              height: 35,
            ),
            Row(
              mainAxisAlignment:MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left:20,top:10,),
                  child: Text(
                    'Your cart',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Montserrat'
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right:20,top:10,),
                  child: Text(
                    '${price.toStringAsFixed(1)} TND',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Montserrat'
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            //items listview
            Container(
              height: MediaQuery.of(context).size.height*0.64,
              padding: EdgeInsets.only(left:10,right: 10),
                child: (cart.length>0 &&cart!=null)
                    ? ListView.builder(
                        itemCount: cart.length,
                        itemBuilder: (BuildContext context, int index) {
                          var item = cart[index];
                          return cart[index]['name']!="Created menu"?Card(
                            child: InkWell(
                            splashColor: Colors.blue.withAlpha(30),
                            onTap: () {
                              
                            },
                            child: Container(
                              height: 150,
                              child:Row(
                                //crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1, 
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(5, 1, 5, 1)
                                        ),
                                        Image(
                                          image: NetworkImage(url+'5012/images/'+item['image']),
                                          fit: BoxFit.fitHeight,
                                          width: 100,
                                          height: 145,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex:3, 
                                    child: Column(
                                      children: <Widget>[
                                        Padding(padding: EdgeInsets.only(top:10)),
                                        Expanded(
                                          child:Row(
                                            crossAxisAlignment :CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                              Expanded(
                                                flex:8,
                                                child: Text(
                                                  '${item['name']}',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize:18,
                                                    color:colorCustom,
                                                    fontFamily: 'Montserrat'
                                                  ),
                                                ),
                                              ),
                                              
                                              Expanded(
                                                flex:2,
                                                child: IconButton(
                                                  icon: Icon(Icons.close),
                                                  color: colorCustom,
                                                  onPressed: () async{
                                                    final prefs = await SharedPreferences.getInstance();
                                                    setState(() {
                                                      cart.removeAt(index);
                                                      prefs.setString('cart', json.encode(cart));
                                                    });
                                                    //cart.removeAt(index);
                                                  },
                                                ),
                                              ),
                                              SizedBox(
                                                width: 2,
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Expanded(
                                          child:Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                              Expanded(child: 
                                                Text(
                                                    '${item['description']}', 
                                                    style:TextStyle(
                                                      fontSize:14,
                                                      fontFamily: 'Montserrat'
                                                    )
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height:5
                                        ),
                                        Expanded(
                                          child:Row(
                                            children: <Widget>[
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                flex: 3,
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text(
                                                      'Price',
                                                      style: TextStyle(
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),
                                                    Text(
                                                      '${double.parse(item['price'])} TND',
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize:16,
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),

                                                  ],
                                                ),
                                              ),
                                              VerticalDivider(
                                                endIndent: 10,
                                                color: CupertinoColors.systemGrey4,
                                                thickness: 1,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                flex: 5,
                                                child: Container(
                                                  padding: EdgeInsets.only(left:10),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      IconButton(
                                                        icon: Icon(Icons.remove),
                                                        iconSize: 30,
                                                        color: colorCustom,
                                                        onPressed: () {
                                                          if(item['quantity']==1){
                                                            setState(() {
                                                              item['quantity']=1;
                                                              /*clicks_counter*/
                                                            });
                                                          }
                                                          else{
                                                            setState(() {
                                                              item['quantity']--;
                                                              price=price - double.parse(item['price']);
                                                            });
                                                          }
                                                        },
                                                      ),
                                                      SizedBox(width:10),
                                                      Text(
                                                        "${item['quantity']}",
                                                        style:TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 16,
                                                          fontWeight: FontWeight.bold,
                                                        ),
                                                      ),
                                                      SizedBox(width: 10),
                                                      IconButton(
                                                        icon: Icon(Icons.add),
                                                        iconSize: 30,
                                                        color: colorCustom,
                                                        onPressed: () {
                                                          setState(() {
                                                            item['quantity']++;
                                                            print(item['quantity']);
                                                            price=price + double.parse(item['price']);
                                                          });
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                           
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  
                                ],
                              )
                            )
                          ),
                        ):
                          Card(
                            child: InkWell(
                            splashColor: Colors.blue.withAlpha(30),
                            onTap: () {
                              
                            },
                            child: Container(
                              height: 150,
                              child:Row(
                                //crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1, 
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(5, 1, 5, 1)
                                        ),
                                        Image(
                                          image: AssetImage('assets/plat.jpg'),
                                          fit: BoxFit.fitHeight,
                                          width: 100,
                                          height: 145,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex:3, 
                                    child: Column(
                                      children: <Widget>[
                                        Padding(padding: EdgeInsets.only(top:10)),
                                        Expanded(
                                          child:Row(
                                            crossAxisAlignment :CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                              Expanded(
                                                flex:8,
                                                child: Text(
                                                  '${item['name']}',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize:18,
                                                    color:colorCustom,
                                                    fontFamily: 'Montserrat'
                                                  ),
                                                ),
                                              ),
                                              

                                              Expanded(
                                                flex:2,
                                                child: IconButton(
                                                  icon: Icon(Icons.close),
                                                  color: colorCustom,
                                                  onPressed: () async{
                                                    final prefs = await SharedPreferences.getInstance();
                                                    setState(() {
                                                      cart.removeAt(index);
                                                      prefs.setString('cart', json.encode(cart));
                                                    });
                                                    //cart.removeAt(index);
                                                  },
                                                ),
                                              ),
                                              SizedBox(
                                                width: 2,
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Expanded(
                                          child:Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5),),
                                              Expanded(child: 
                                                Text(
                                                    '${descriptionMaker(item['ingredients'])}', 
                                                    style:TextStyle(
                                                      fontSize:14,
                                                      fontFamily: 'Montserrat'
                                                    )
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height:5
                                        ),
                                        Expanded(
                                          child:Row(
                                            children: <Widget>[
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                flex: 3,
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text(
                                                      'Price',
                                                      style: TextStyle(
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),
                                                    Text(
                                                      '${item['price']} TND',
                                                      overflow: TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize:14,
                                                        color:CupertinoColors.systemGrey,
                                                        fontFamily: 'Montserrat'
                                                      ),
                                                    ),

                                                  ],
                                                ),
                                              ),
                                              VerticalDivider(
                                                endIndent: 10,
                                                color: CupertinoColors.systemGrey4,
                                                thickness: 1,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                flex: 5,
                                                child: Container(
                                                  padding: EdgeInsets.only(left:10),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      IconButton(
                                                        icon: Icon(Icons.remove),
                                                        iconSize: 30,
                                                        color: colorCustom,
                                                        onPressed: () {
                                                          if(item['quantity']==1){
                                                            setState(() {
                                                              item['quantity']=1;
                                                              /*clicks_counter*/
                                                            });
                                                          }
                                                          else{
                                                            setState(() {
                                                              item['quantity']--;
                                                              price=price - double.parse(item['price']);
                                                            });
                                                          }
                                                        },
                                                      ),
                                                      SizedBox(width:10),
                                                      Text(
                                                        "${item['quantity']}",
                                                        style:TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontSize: 16,
                                                          fontWeight: FontWeight.bold,
                                                        ),
                                                      ),
                                                      SizedBox(width: 10),
                                                      IconButton(
                                                        icon: Icon(Icons.add),
                                                        iconSize: 30,
                                                        color: colorCustom,
                                                        onPressed: () {
                                                          setState(() {
                                                            item['quantity']++;
                                                            print(item['quantity']);
                                                            price=price + double.parse(item['price']);
                                                          });
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        
                                      ],
                                    ),
                                  ),
                                            
                                ],
                              )
                            )
                          ),
                        );
                    })
                : Center(
                child: CircularProgressIndicator(),
              ),
            ),
          
            //checkout button
            Container(
              margin: EdgeInsets.only(left:10,right:10),
              color: colorCustom,
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: Align(
                alignment: FractionalOffset.center,
                child: InkWell(
                  splashColor: Colors.blue.withAlpha(30),
                  borderRadius: BorderRadius.all(Radius.circular(120)),
                  onTap: () async{
                  },
                  child:Text(
                    'Checkout',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Montserrat',
                      fontSize: 18
                    ),
                  ),
                          
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            //empty button
            Container(
              margin: EdgeInsets.only(left:10,right:10),
              color: colorCustom,
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: Align(
                alignment: FractionalOffset.center,
                child: InkWell(
                  splashColor: Colors.blue.withAlpha(30),
                  borderRadius: BorderRadius.all(Radius.circular(120)),
                  onTap: () async{
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    print(prefs.getString('cart'));  
                    //prefs.remove('cart');
                    setState(() {
                      prefs.remove('cart');
                      prefs.clear();
                    });  
                  },
                  child:Text(
                    'Empty',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Montserrat',
                      fontSize: 18
                    ),
                  ),
                          
                ),
              ),
            ),
    
          ],
        ):
        Column(
          children: <Widget>[
            SizedBox(
              height: 35,
            ),
            Container(
              margin: EdgeInsets.only(left:10),
              child: Text(
              'Cart empty',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Mo ntserrat',
                  fontSize: 22
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


