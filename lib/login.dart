import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'styles.dart';
import 'home.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'http_service.dart';
import 'package:shared_preferences/shared_preferences.dart';



class Login extends StatefulWidget {
  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> { 
  final email = TextEditingController();
  final password=TextEditingController();
  
  bool loader=false;
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed. 
    email.dispose();
    password.dispose();
    super.dispose();
  }

  @override 
  Widget build (BuildContext context) { 
  MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);

    return Scaffold (
      body: Center(
        child: SingleChildScrollView( 
          child:Column(
            children: <Widget>[
              Image(
                image: AssetImage('assets/logo-yellow.png'),
                width: 200.0,
              ),

              SizedBox(
                height:75.0,
              ),

              Container(
                margin: EdgeInsets.only(left: 10,right: 10),
                child:CupertinoTextField(
                  controller: email,
                  placeholder: 'Email..',
                  padding: EdgeInsets.all(16.0),
                ),
              ),
                
              SizedBox(
                height: 15,
              ),

              Container(
                margin: EdgeInsets.only(left: 10,right: 10),
                child:CupertinoTextField(
                  obscureText: true,
                  controller: password,
                  placeholder: 'password..',
                  padding: EdgeInsets.all(16.0),
                ),
              ),
              
              SizedBox(
                height:75.0,
              ),
              loader?CircularProgressIndicator():
              CupertinoButton(

                onPressed: ()async{
                  setState(() {
                    loader=true;
                  });
                  final prefs = await SharedPreferences.getInstance();
                  //navigateToHome(context);
                  print(email.text);
                  print(password.text);
                  
                  await signIn(email.text,password.text);
                  if(prefs.getString('user')==null){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  }
                  else{

                    Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                  }
                  setState(() {
                    loader=false;
                  });
                },
                child: Text('Sign in',style:testStyle),
                color: colorCustom,
                borderRadius: new BorderRadius.circular(50.0),
                padding: EdgeInsets.fromLTRB(100.0, 0.0, 100.0, 0.0),
              ),
              SizedBox(
                height:25.0,
              ),
              Text(
                "don't have an account ?",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 18
                ),
              ),
              CupertinoButton(
                onPressed: (){
                  //navigateToSignUp(context);
                },
                child: Text('Sign Up',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 18
                  ),
                ),

              ), 
            ],
          )
        ),
      ),
    );
  } 
}