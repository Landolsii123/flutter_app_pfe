import 'package:flutter/cupertino.dart' ;
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'styles.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home.dart';
import 'dart:convert';
import 'http_service.dart';
import 'add_measures.dart';
import 'package:intl/intl.dart';

class Profile extends StatefulWidget {
  @override
  _Profile createState() => _Profile();
}

class _Profile extends State<Profile>{

  var user;
  double calories=0;
  List<charts.Series<Measures, int>> _seriesLineData;
  List<Measures> lineweightdata=[];
  String currentDay = DateFormat('EEEE').format(DateTime.now());
  String formattedDate = DateFormat('d').format(DateTime.now());

  _generateData() {
    
    
    
    _seriesLineData.add(
      charts.Series(
        colorFn: (__, _) => charts.ColorUtil.fromDartColor(Color(0xff990099)),
        id: 'Weight',
        data: lineweightdata,
        domainFn: (Measures weeks, _) => weeks.week,
        measureFn: (Measures weights, _) => weights.weight,
      ),
    );
    
  }

  getUserFromSharedPreferences() async{
    final prefs = await SharedPreferences.getInstance();
    var user = prefs.getString('user');
    print('user before return: '+json.decode(user)['id']);
    return json.decode(user)['id'];
  }

  @override
  void initState() {
    
    _seriesLineData = List<charts.Series<Measures, int>>();
    
    Future.delayed(const Duration(seconds: 0),(){
      
    
    getUserFromSharedPreferences().then((id) {
      print('after call of function : '+id);
      
      getUser(id).then((userr){
        print(json.encode(userr));
        setState(() {
          user= userr;
          if(user['measures'].length>0){
            if(user['gender']=='Male'){
              calories=((13.7526 * double.parse(user['measures'][0]['weight'])) + (500.33 * (double.parse(user['measures'][0]['height']))/100) - (6.7550 * 25) + 66.473);
            }
            else{
              calories=((9.5634 * double.parse(user['measures'][0]['weight'])) + (184.96 * (double.parse(user['measures'][0]['height']))/100) - (4.6756 * 25) + 655.0955);
            }
          }
            for(int i =0;i<user['measures'].length;i++){
               lineweightdata.add(Measures(i, double.parse(user['measures'][i]['weight'])));
            }
           
          _generateData();
        });
      });
    });
    });
    
    super.initState();
  }

  @override 
  Widget build (BuildContext context){
    MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    
    return Scaffold(
      body: SingleChildScrollView( 
        child: (user != null)&&(user.length!=0) ? 
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //yellow COntainer
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height*0.3,
              child: Stack(
                children:<Widget>[ 
                  Container(
                    decoration: BoxDecoration(
                      color: colorCustom,
                      //borderRadius: BorderRadius.only(bottomLeft:Radius.circular(50),bottomRight: Radius.circular(50))
                    ),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height*0.3,
                  ),
                  //name
                  Align(
                    alignment: Alignment(-1,-1),
                    child:Container(
                      margin: EdgeInsets.only(left:35,top:50),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      ),
                      child:Text(
                        '${user['firstname']} ${user['lastname']}',
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat'
                        ),
                      ),
                    ),
                  ),
                  //mail
                  Align(
                    alignment: Alignment(-1,-0.1),
                    child:Container(
                      margin: EdgeInsets.only(left:40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      ),
                      child:Text(
                        "${user['email']}",
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'Montserrat'
                        ),
                      ),
                    ),
                  ),
                  //phone number
                  Align(
                    alignment: Alignment(-1,0.1),
                    child:Container(
                      margin: EdgeInsets.only(left:40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      ),
                      child:Text(
                        "${user['phonenumber']}",
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'Montserrat'
                        ),
                      ),
                    ),
                  ),
                  //birthday
                  Align(
                    alignment: Alignment(-1,0.3),
                    child:Container(
                      margin: EdgeInsets.only(left:40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      ),
                      child:Text(
                        "${user['birthday']}",
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'Montserrat'
                        ),
                      ),
                    ),
                  ),
                  //size
                  Align(
                    alignment: Alignment(-1,0.5),
                    child:Container(
                      margin: EdgeInsets.only(left:40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      ),
                      child:Text(
                        user['measures'].length> 0?
                        "Size : ${user['measures'][0]['height']} cm":
                        "Size : Unknown",
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'Montserrat'
                        ),
                      ),
                    ),
                  ),
                  //weight
                  Align(
                    alignment: Alignment(-1,0.7),
                    child:Container(
                      margin: EdgeInsets.only(left:40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      ),
                      child:Text(
                        user['measures'].length>0?
                        "Weight : ${user['measures'][0]['weight']} Kg":
                        "Weight : Unknown",
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'Montserrat'
                        ),
                      ),
                    ),
                  ),
                  // Logout
                  Align(
                    alignment: Alignment(1,-0.5),
                    child:Container(
                      margin: EdgeInsets.only(left:40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      ),
                      child:IconButton(
                        icon: Icon(Icons.directions_run), 
                        iconSize: 35,
                        onPressed: () async{
                          SharedPreferences prefs = await SharedPreferences.getInstance();
                          
                          print(prefs.getString('user'));  
                          prefs.clear();
                          
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));                       
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            user['measures'].length>0 ?
            Column(
              children: <Widget>[
                //labels(calories,proteins,fats,carbohydrate)
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        'Calories',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          fontSize: 14
                        ),
                      ),
                      
                    )
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        'Proteins',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          fontSize: 14
                        ),
                      ),
                      
                    )
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        'Fats',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          fontSize: 14
                        ),
                      ),
                      
                    )
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        'Carbohydrate',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          fontSize: 14
                        ),
                      ),
                      
                    )
                  ),
                
                ],
              ),
              //SizedBox(height: 5,),
              //cercles
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      width: 20,
                      margin: EdgeInsets.all(15),
                      padding: EdgeInsets.all(17),
                      decoration: BoxDecoration(
                        
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        border: Border.all(
                          color:CupertinoColors.systemGrey,
                          width: 2,
                        ),
                      ),
                      alignment: Alignment.center,
                      child: Column(
                        children: <Widget>[
                          //Homme = [13,7516 x Poids (kg)] + [500,33 x Taille (m)] – (6,7550 x Age) + 66,473
                          //Femme = [9,5634 x Poids (kg)] + [184,96 x Taille (m)] – (4,6756 x Age) + 655,0955
                          Text(
                            '${calories.toStringAsFixed(0)}',                          
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold,
                              fontSize: 10
                            ),
                          ),
                          Text(
                            'Cal',
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                fontSize: 10,
                                color: CupertinoColors.systemGrey
                              ),
                            ),
                      
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(15),
                      padding: EdgeInsets.all(17),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        border: Border.all(
                          color:CupertinoColors.systemGrey,
                          width: 2
                        ),
                      ),
                      alignment: Alignment.center,
                      child: Column(
                        children: <Widget>[
                          //70 kg x 1.7 = 119 grams protein per day
                          Text(
                            '${(double.parse(user['measures'][0]['weight']) * 1.7).toStringAsFixed(1)}',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold,
                              fontSize: 10
                            ),
                          ),
                          Text(
                            'gr',
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                fontSize: 10,
                                color: CupertinoColors.systemGrey
                              ),
                            ),
                      
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(15),
                      padding: EdgeInsets.all(17),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        border: Border.all(
                          color:CupertinoColors.systemGrey,
                          width: 2
                        ),
                      ),
                      alignment: Alignment.center,
                      child: Column(
                        children: <Widget>[
                          Text(
                            '${((calories * 0.2)/9).toStringAsFixed(0)}',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold,
                              fontSize: 10
                            ),
                          ),
                          Text(
                            'gr',
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                fontSize: 10,
                                color: CupertinoColors.systemGrey
                              ),
                            ),
                      
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(15),
                      padding: EdgeInsets.all(17),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        border: Border.all(
                          color:CupertinoColors.systemGrey,
                          width: 2
                        ),
                      ),
                      alignment: Alignment.center,
                      child: Column(
                        children: <Widget>[
                          Text(
                            '${((calories/2)/4).toStringAsFixed(0)}',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold,
                              fontSize: 10
                            ),
                          ),
                          Text(
                            'gr',
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                fontSize: 10,
                                color: CupertinoColors.systemGrey
                              ),
                            ),
                      
                        ],
                      ),
                    ),
                  ),
                  
                ],
              ),
              SizedBox(
                height: 10,
              ),
              //ideal Weight
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left:20),
                    child: Text(
                      'Ideal Weight :',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                        color: CupertinoColors.systemGrey
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left:5),
                    //Poids idéal d’un homme (en Kg) = Taille (en cm) - 100 - ((Taille (en cm) - 150) /4 ).
                    //Poids idéal d’une femme (en Kg) = Taille (en cm) - 100 - ((Taille (en cm) - 150) /2,5 ).
                    child: Text(
                      user['gender']=='Male'?
                      '${(double.parse(user['measures'][0]['height']) - 100 - ((double.parse(user['measures'][0]['height'])-150)/4)).toStringAsFixed(2)}':
                      '${(double.parse(user['measures'][0]['height']) - 100 - ((double.parse(user['measures'][0]['height'])-150)/2.5)).toStringAsFixed(2)}',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat'
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              //Water needed
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left:20),
                    child: Text(
                      'Water Needed :',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                        color: CupertinoColors.systemGrey
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left:5),
                    child: Text(
                      '${(((double.parse(user['measures'][0]['weight'])* 2.2) * 2/3) *0.02957).toStringAsFixed(1)} L/day',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat'
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),

              user['measures'].length>1?
              //Chart  
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 200,
                child: charts.LineChart(
                  _seriesLineData,
                  defaultRenderer: new charts.LineRendererConfig(
                      includeArea: true, stacked: true),
                  animate: true,
                  animationDuration: Duration(seconds: 1),
                  behaviors: [
                    new charts.ChartTitle('Weeks',
                        behaviorPosition: charts.BehaviorPosition.bottom,
                        titleOutsideJustification:charts.OutsideJustification.middleDrawArea),
                    new charts.ChartTitle('Weight',
                        behaviorPosition: charts.BehaviorPosition.start,
                        titleOutsideJustification: charts.OutsideJustification.middleDrawArea),
                      
                  ]
                ),
              )
              :
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(10),
                child: Text(
                  'You will get the tracking chart once you have entered measures two times at least',
                  textAlign: TextAlign.justify,
                  
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
              //(currentDay=='Sunday')&&(int.parse(user['measures'][user['measures'].length-1]['date'].substring(8,10))!=int.parse(formattedDate))?
                Center(
                  child: CupertinoButton(
                    child: Text(
                      'Add new measures',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ), 
                    onPressed:(){
                      Navigator.push(context, CupertinoPageRoute(
                        builder: (context) =>AddMeasures(),
                        settings: RouteSettings(arguments: user['_id']),
                        )
                      ); 
                    } 
                  ),
              
                ),
                //Text(""),
              ],
            ):
            Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    'Enter your first measures and start your diet',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 16,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  alignment: Alignment.center,
                  child: Text(
                    'your measures will be calculated with specific algorithms and you will get your needs of calories, proteins, fats, carbohydrates, water needed per day and ideal weight.',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                    ),
                  ),
                ),
                
                Container(
                  padding: EdgeInsets.all(10),
                  alignment: Alignment.center,
                  child: Text(
                    'Also using Healthy Food Locator is free and it will always be.',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  alignment: Alignment.center,
                  child: CupertinoButton(
                    onPressed: () {
                      Navigator.push(context, CupertinoPageRoute(
                      builder: (context) =>AddMeasures(),
                      settings: RouteSettings(arguments: user['_id']),
                    )
                  ); 
                    },
                    child: Text(
                      'Get Started',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
              ],
            ),
            
          ],
        ):
        Center(
          child:CircularProgressIndicator(),
        ),
      ), 
    );
  }
}


class Measures {
  int week;
  double weight;

  Measures(this.week, this.weight);
}

