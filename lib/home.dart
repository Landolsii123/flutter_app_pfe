/*import 'package:flutter/material.dart';
import 'package:healthy_food_locator/all_restaurants.dart';
import 'package:healthy_food_locator/login.dart';
import 'package:healthy_food_locator/shopping_cart.dart';
import 'package:healthy_food_locator/user_profile.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'custom-widget.dart';
import 'login.dart';
import 'restaurant_locator.dart';
import 'home.dart';
import 'styles.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'http_service.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Home> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var loggedIn;
  MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
  PersistentTabController _controller;
  bool x = true;
  
  logg()async {
    loggedIn= await _prefs.then((SharedPreferences prefs) {
      return (prefs.getString('user'));
    });
  }

  @override
  void initState() {
    super.initState();
    logg();
    _controller = PersistentTabController(initialIndex: 0);
  }

  List<Widget> _buildScreens() {
    return [
      RestaurantLocator(),
      AllRestaurants(),
      ShoppingCart(),
      loggedIn==null?Login():Profile(),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(Icons.home),
        title: ("Home"),
        activeColor: colorCustom,
        inactiveColor: Colors.grey,
        isTranslucent: false,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.restaurant),
        title: ("Restaurants"),
        activeColor: colorCustom,
        inactiveColor: Colors.grey,
        isTranslucent: false,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.shopping_basket),
        title: ("Restaurants"),
        activeColor: colorCustom,
        inactiveColor: Colors.grey,
        isTranslucent: false,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.person),
        title: loggedIn==null?('Login'):('Profile'),
        activeColor: colorCustom,
        inactiveColor: Colors.grey,
        isTranslucent: false,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
        controller: _controller,
        screens: _buildScreens(),
        items:
            _navBarsItems(), // Redundant here but defined to demonstrate for other than custom style
        //confineInSafeArea: true,
        backgroundColor: Colors.white,
        handleAndroidBackButtonPress: true,
        onItemSelected: (int) {
          setState(
              () {}); // This is required to update the nav bar if Android back button is pressed
        },
        customWidget: CustomNavBarWidget(
          items: _navBarsItems(),
          onItemSelected: (index) {
            setState(() {
              _controller.index = index; // THIS IS CRITICAL!! Don't miss it!
            });
          },
          selectedIndex: _controller.index,
        ),
        itemCount: 4,
        navBarStyle:
            NavBarStyle.custom // Choose the nav bar style with this property
        );
  }
}


*/


import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:healthy_food_locator/home_restaurants.dart';
import 'package:healthy_food_locator/shopping_cart.dart';
import 'styles.dart';
import 'login.dart';
import 'all_restaurants.dart';
import 'restaurant_locator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'http_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:healthy_food_locator/user_profile.dart';
import 'dart:async';
import 'add_measures.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<Home> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  int _selectedIndex = 0;
  static const TextStyle optionStyle =TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  var loggedIn;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  

  logg()async {
    loggedIn= await _prefs.then((SharedPreferences prefs) {
      return (prefs.getString('user'));
    });
  }

  @override
  void initState() {
    super.initState();
    logg();
  }

  @override
  Widget build(BuildContext context){
    MaterialColor colorCustom = MaterialColor(0xFFffdd00, color);
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: _selectedIndex == 0 ? RestaurantLocator() 
        :_selectedIndex == 1 ? AllRestaurants(): _selectedIndex == 2 ? ShoppingCart():loggedIn==null?Login():Profile()
      ),
      bottomNavigationBar: CupertinoTabBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.restaurant),
            title: Text('Restaurants'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text('Shpping Cart'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Profile'),
          ),
        ],
        currentIndex: _selectedIndex,
        //selectedItemColor: Colors.amber[800],
        activeColor: colorCustom,
        onTap: _onItemTapped,
      ),
    );
  }
}


