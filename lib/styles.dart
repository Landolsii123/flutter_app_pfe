import 'package:flutter/material.dart';

Map<int, Color> color =
{
50:Color.fromRGBO(18,161,154, .1),
100:Color.fromRGBO(18,161,154, .2),
200:Color.fromRGBO(18,161,154, .3),
300:Color.fromRGBO(18,161,154, .4),
400:Color.fromRGBO(18,161,154, .5),
500:Color.fromRGBO(18,161,154, .6),
600:Color.fromRGBO(18,161,154, .7),
700:Color.fromRGBO(18,161,154, .8),
800:Color.fromRGBO(18,161,154, .9),
900:Color.fromRGBO(18,161,154, 1),
};

const TextStyle testStyle = const TextStyle(
  fontFamily: 'Verdana',
  fontSize: 20.0,
);